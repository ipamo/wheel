﻿const gulp = require('gulp');
const del = require('del');
const DOMParser = require('xmldom').DOMParser;
const xpath = require('xpath');
const fs = require('fs');
const path = require('path');
const os = require('os');
const cp = require('child_process');

const build_dir = 'build';
const nuget_packages = path.join(os.homedir(), '.nuget', 'packages');
const nuget_localrepo = path.join(os.homedir(), '.nuget', 'localrepo');
const nuget_localrepo_symbols = path.join(os.homedir(), '.nuget', 'localrepo-symbols');
const msbuild = searchMsbuild();

function searchMsbuild() {
	let msbuild = "C:\\Program Files (x86)\\Microsoft Visual Studio\\2019\\Community\\MSBuild\\Current\\Bin\\MSBuild.exe";
	console.log(msbuild);
	if (fs.existsSync(msbuild))
		return msbuild;
	
	msbuild = "C:\\Program Files (x86)\\Microsoft Visual Studio\\2017\\Professional\\MSBuild\\15.0\\Bin\\MSBuild.exe";
	if (fs.existsSync(msbuild))
		return msbuild;

	msbuild = "C:\\Program Files (x86)\\Microsoft Visual Studio\\2017\\Enterprise\\MSBuild\\15.0\\Bin\\MSBuild.exe";
	if (fs.existsSync(msbuild))
		return msbuild;

	msbuild = "C:\\Program Files (x86)\\Microsoft Visual Studio\\2017\\Community\\MSBuild\\15.0\\Bin\\MSBuild.exe";
	if (fs.existsSync(msbuild))
		return msbuild;

	return undefined;
}

function runSync(command) {
	try {
		cp.execSync(command, {stdio: ['inherit', 'inherit', 'inherit']});
		return 0;
	} catch (error) {
		return error.status;
	}
}

function getVersion(projectDir) {
	const projectFile = path.join(projectDir, path.basename(projectDir) + '.csproj');
	var data = fs.readFileSync(projectFile, 'utf-8');
    var doc = new DOMParser().parseFromString(data, 'application/xml');
	var version = xpath.select("/Project/PropertyGroup/Version/text()", doc)[0].data;
	return version;
}

function buildWithMsbuild(projectDir) {
	if (! msbuild)
		throw new Error("Msbuild not found");
	runSync(`"${msbuild}" "${projectDir}" /t:Pack /p:Configuration=Release /verbosity:minimal /p:PackageOutputPath="..\\..\\${build_dir}" /p:BuildProjectReferences=false`);
}

function buildWithDotnetCli(projectDir) {
//	
}

function build() {
	if (runSync("dotnet restore") != 0)
		return;
	buildWithMsbuild("src/Wheel.Backports");
}

gulp.task('build', function(cb) {
	build();

//	let version = getVersion("src/Wheel.Backports");
//	console.log(version);

	/*
	set method=dotnet
	if "!project!" == "Wheel" set method=msbuild
	if "!project!" == "Wheel.Plus" set method=msbuild
	if "!project!" == "Wheel.Backports" set method=msbuild
	if "!project!" == "Wheel.Plus.Backports" set method=msbuild
	if "!project!" == "Wheel.Commands" set method=msbuild
	if "!project!" == "Wheel.Ftp" set method=msbuild
	
	:: ------------------------------------------
	:: Pack
	call :old "!project!.!version!.nupkg" || exit /b 1
	
	if "!method!" == "msbuild" (
		if not exist "!msbuild!" (
			:: Check presence of msbuild
			echo NOT FOUND: MSBuild.exe.
			exit /b 1
		)
		"!msbuild!" "src\!project!" /t:Pack /p:Configuration=Release /verbosity:minimal /p:PackageOutputPath="..\..\!build_dir!" /p:BuildProjectReferences=false
	) else (
		dotnet pack "src\!project!" -c Release -o "..\..\!build_dir!" --no-dependencies
	)
	*/

	cb();
});

gulp.task('clean', function() {
	return del([
		"**/bin/",
		"**/obj/",
		"**/logs/",
		"**/*.user",
		"**/*.dbmdl",
		"**/*.jfm",
        "**/.vs", 
        "**/.vscode",
		"**/wwwroot/css/*.min.css",
		"**/wwwroot/js/*.min.js",
		"**/wwwroot/lib",
        // Exclude node_modules and .git
		"!node_modules/**",
		"!.git/**",
		"!.vscode/settings.json"
	], {dryRun: false})
	.then(paths => {
		console.log('Deleted: ' + (paths.length ? '\n' + paths.join('\n') : 'none'));
	});
});
