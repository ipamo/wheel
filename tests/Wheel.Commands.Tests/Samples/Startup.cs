﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Wheel.Commands.Tests.Samples
{
    class Startup
    {
        public void ConfigureCommands(ICommandBuilder root)
        {
            root.DefaultCommand<StaticCommand>();
            root.Command<DirectCommand>();
            root.Command<ThreadedCommand>();
            root.AddCommands();
            root.Command("hello", cmd =>
            {
                cmd.Description = "Say hello";
                var toArg = cmd.Argument("to", "Who do you want to say hello to?");
                cmd.Action = () =>
                {
                    Console.WriteLine("Hello, {0}", toArg.HasValue ? toArg.Value : "world");
                };
            });
        }
    }
}
