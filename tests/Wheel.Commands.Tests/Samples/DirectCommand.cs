﻿using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;

namespace Wheel.Commands.Tests.Samples
{
    class DirectCommand
    {
        public static void ConfigureCommand(ICommandBuilder cmd, IServiceCollection services)
        {
            services.AddTransient<DirectCommand>();

            var numOpt = cmd.Option("-n|--num", "Num", required: true);
            cmd.Action = () =>
            {
                var o = cmd.Services.GetRequiredService<DirectCommand>();
                o.Run(numOpt.IntValue);
            };
        }

        private readonly ILogger _logger;

        public DirectCommand(ILogger<DirectCommand> logger)
        {
            _logger = logger;
        }

        public void Run(int num)
        {
           _logger.LogError($"Test direct {num}");
        }
    }
}
