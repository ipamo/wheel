﻿using System;
using Microsoft.Extensions.Configuration;
using System.Collections.Generic;
using System.Linq;
using System.Diagnostics;

namespace Wheel.Commands.Tests
{
    /// <summary>
    /// The goal of this class is is to determine whether tests with extended Fact and Theory will be skipped.
    /// <para>
    /// Note: discovery of tests with the extended attributes will fail in case of exception.
    /// If this happens, TestsInit will also fail and display the TypeInitializationException.
    /// </para>
    /// </summary>
    internal static class TestsInit
    {
        static TestsInit()
        {
            var infra = WheelInfraBuilder.CreateDefaultBuilder().Build();
            Configuration = infra.Configuration;
            SetSkipReport();
        }

        public static IConfiguration Configuration { get; }
        public static string SkipReport { get; private set; }
        
        private static void SetSkipReport()
        {
            if (Configuration.GetValue<bool>("Tests:SkipReport"))
            {
                SkipReport = "Tests:SkipReport config is set";
                return;
            }
            if (string.IsNullOrWhiteSpace(Configuration["Smtp:Host"]))
            {
                SkipReport = "Smtp:Host config is not set";
                return;
            }
            if (string.IsNullOrWhiteSpace(Configuration["CommandApplication:ReportEmailTo"]))
            {
                SkipReport = "CommandApplication:ReportEmailTo config is not set";
                return;
            }
        }
    }
}
