﻿using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace Wheel.Commands.Tests
{
    class CssInliner : ICssInliner
    {
        private readonly string _cssFile;
        private readonly ILogger _logger;

        public CssInliner(ILogger<CssInliner> logger)
        {
            _logger = logger;

            var minFile = "email.min.css";
            var nonMinFile = "email.css";
            if (File.Exists(minFile))
                _cssFile = File.ReadAllText(minFile);
            else
            {
                if (File.Exists(nonMinFile))
                    _cssFile = File.ReadAllText(nonMinFile);
                else
                    _logger.LogWarning($"Fichier introuvable : {nonMinFile} ou {minFile}");
            }
        }
        
        public string Inline(string html)
        {
            if (_cssFile == null)
                return html;

            var result = PreMailer.Net.PreMailer.MoveCssInline(html, css: _cssFile);
            foreach (var warn in result.Warnings)
            {
                _logger.LogWarning(warn);
            }
            return result.Html;
        }
    }
}
