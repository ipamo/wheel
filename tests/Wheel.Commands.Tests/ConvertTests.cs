﻿using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using Wheel.Commands.Internal;
using Xunit;

namespace Wheel.Commands.Tests
{
    public class ConvertTests
    {
        [Fact]
        public void ToScalar()
        {
            Assert.Equal(new DateTime(1998, 7, 12), CommandTypeWrapper.ConvertTo("1998-07-12", typeof(DateTime?)));
            Assert.Equal(new DateTime(1998, 7, 12), CommandTypeWrapper.ConvertTo("1998-07-12", typeof(DateTime)));
        }

        [Fact]
        public void ToScalar_EchecDate()
        {
            try
            {
                Assert.Equal(new DateTime(1998, 7, 12), CommandTypeWrapper.ConvertTo("1XXX-07-12", typeof(DateTime?)));
                Assert.True(false);
            }
            catch (InvalidOperationException)
            {
                Assert.True(true);
            };
        }

        [Fact]
        public void ToScalar_EchecInt()
        {
            try
            {
                Assert.Equal(2, CommandTypeWrapper.ConvertTo("Z", typeof(int)));
                Assert.True(false);
            }
            catch (InvalidOperationException)
            {
                Assert.True(true);
            };
        }

        [Fact]
        public void ToEnum()
        {
            Assert.Equal(LogLevel.Debug, CommandTypeWrapper.ConvertTo("Debug", typeof(LogLevel)));
            Assert.Equal(LogLevel.Debug, CommandTypeWrapper.ConvertTo("Debug", typeof(LogLevel?)));
            Assert.Equal(LogLevel.Debug, CommandTypeWrapper.ConvertTo("deBUG", typeof(LogLevel))); ;
            Assert.Equal(LogLevel.Debug, CommandTypeWrapper.ConvertTo("deBUG", typeof(LogLevel?)));

            Assert.Equal(LogLevel.Debug, CommandTypeWrapper.ConvertTo("1", typeof(LogLevel)));
            Assert.Equal(LogLevel.Debug, CommandTypeWrapper.ConvertTo("1", typeof(LogLevel?)));
        }

        [Fact]
        public void ToEnum_Echec()
        {
            try
            {
                Assert.Equal(LogLevel.Debug, CommandTypeWrapper.ConvertTo("Debugx", typeof(LogLevel)));
                Assert.True(false);
            }
            catch (InvalidOperationException)
            {
                Assert.True(true);
            };
        }
    }
}