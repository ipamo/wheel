﻿using System;
using System.Collections.Generic;
using System.Text;
using Wheel.Internal;
using Xunit;

namespace Wheel.Tests
{
    public class InfraTests
    {
        [Fact]
        public void Check()
        {
            Assert.Equal("Wheel.Tests", ReflectionUtils.GetApplicationAssembly().GetName().Name);

            var infra = WheelInfraBuilder.CreateDefaultBuilder().Build();
            Assert.Equal("Testing", infra.Environment.EnvironmentName);
            Assert.Equal("Wheel.Tests", infra.Environment.ApplicationName);
        }
    }
}
