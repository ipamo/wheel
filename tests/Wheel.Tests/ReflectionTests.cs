using System;
using System.Collections.Generic;
using Xunit;

namespace Wheel.Tests
{
    public class ReflectionTests
    {
        [Fact]
        public void PrepareFuncParams()
        {
            var method = typeof(ReflectionTests).GetMethod(nameof(Method));
            var availableParams = new Dictionary<Type, object>
            {
                { typeof(string), "hello" },
                { typeof(int), new Func<int>(() => 2 + 2) }
            };
            var preparedParams = ReflectionUtils.PrepareParameters(method, availableParams);
            var result = method.Invoke(null, preparedParams);
            Assert.Equal("hello: 4", result);
        }

        public static string Method(string name, int num)
        {
            return $"{name}: {num}";
        }
    }
}
