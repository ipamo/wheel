﻿using Xunit;

namespace Wheel.Data.Tests
{
    /// <summary>
    /// A xunit fact skipped if settings directive "Tests:SkipDb" is enabled.
    /// </summary>
    public class DbFactAttribute : FactAttribute
    {
        public DbFactAttribute(string cskey = null)
            : base()
        {
            Skip = TestsInit.SkipDb;
        }
    }
}
