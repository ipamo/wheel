﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Wheel.Data.Tests.DbContextIntegration.Models
{
    public class Post
    {
        public int Id { get; set; }

        [Required]
        public string Title { get; set; }

        [DataType(DataType.MultilineText)]
        public string Content { get; set; }

        public DateTime InsertedAt { get; set; }
        public int InsertedById { get; set; }
        public User InsertedBy { get; set; }
    }
}
