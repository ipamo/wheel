﻿using Microsoft.EntityFrameworkCore;
using System.Linq;
using Microsoft.Extensions.Logging;
using System;
using System.Threading.Tasks;
using System.Threading;
using Microsoft.EntityFrameworkCore.ChangeTracking;
using System.Data.Common;
using System.Security.Claims;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Wheel.Data;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Wheel.Data.Tests.Models;

namespace Wheel.Data.Tests
{
    /// <summary>
    /// A DbContext for tests.
    /// </summary>
    public class TestsDbContext : IdentityDbContext<User, Role, long>
    {
        protected readonly IDbms _dbms;
        private readonly DbContextHelper _helper;

        public DbSet<Component> Components { get; set; }
        public DbSet<Dict> Dicts { get; set; }

        public TestsDbContext(DbContextOptions<TestsDbContext> options, IDbms dbms)
            : base(options)
        {
            _dbms = dbms;
            _helper = new DbContextHelper(dbms);
        }
        
        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);
            _helper.OnModelCreating(builder);
        }

        public override int SaveChanges(bool acceptAllChangesOnSuccess)
        {
            _helper.SaveChanges_AssignEdition(ChangeTracker);
            return base.SaveChanges(acceptAllChangesOnSuccess);
        }

        public override async Task<int> SaveChangesAsync(bool acceptAllChangesOnSuccess, CancellationToken cancellationToken = default(CancellationToken))
        {
            _helper.SaveChanges_AssignEdition(ChangeTracker);
            return await base.SaveChangesAsync(acceptAllChangesOnSuccess, cancellationToken);
        }
    }
}
