﻿using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using System;
using Wheel.Data.Tests.Models;
using Xunit;

namespace Wheel.Data.Tests
{
    //
    // Fixture collection classes have no code, and are never created. Their purpose is
    // simply to be the place to apply [CollectionDefinition] and all the
    // ICollectionFixture<> interfaces.
    //
    // These classes must be public and must be in the same assembly as the
    // test classes using it.
    //
    [CollectionDefinition(nameof(FixtureCollection))]
    public class FixtureCollection : ICollectionFixture<Fixture>
    {
       
    }

    public class Fixture
    {
        public Fixture()
        {
            var infra = WheelInfraBuilder.CreateDefaultBuilder().Build();
            
            var connectionString = TestsInit.Configuration.GetConnectionString("Default");
            if (connectionString.Contains("Integrated Security"))
            {
                infra.ServiceCollection.AddSingleton<IDbms, SqlServerDbms>();
                infra.ServiceCollection.AddDbContext<TestsDbContext>(builder => builder.UseSqlServer(connectionString));
            }
            else
            {
                infra.ServiceCollection.AddSingleton<IDbms, PostgreSqlDbms>();
                infra.ServiceCollection.AddDbContext<TestsDbContext>(builder => builder.UseNpgsql(connectionString));
            }

            infra.ServiceCollection.AddIdentityCore<User>()
                .AddRoles<Role>()
                .AddEntityFrameworkStores<TestsDbContext>();

            Services = infra.BuildApplicationServices();
        }

        public IServiceProvider Services { get; }
    }
}
