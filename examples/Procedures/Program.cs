﻿using Microsoft.Extensions.Configuration;
using System;
using Wheel;
using Wheel.Data;

namespace ConsoleApp1
{
    class Program
    {
        static void Main(string[] args)
        {
            var infra = WheelInfraBuilder.CreateDefaultBuilder().Build();
            var connectionString = infra.Configuration.GetConnectionString("Default");

            IDbms dbms;
            if (connectionString.Contains("Integrated Security"))
                dbms = new SqlServerDbms(infra.Configuration, infra.LoggerFactory);
            else
                dbms = new PostgreSqlDbms(infra.Configuration, infra.LoggerFactory);

            TestReturn(dbms);
            TestOut(dbms);

            Console.WriteLine("Press any key to terminate...");
            Console.ReadKey();
        }

        private static void TestReturn(IDbms dbms)
        {
            string name;
            name = "fn_essai_return";
            var ret = dbms.Exec(name, new object[] { "World" });
            Console.WriteLine("{0} returned value: {1}", name, ret);
        }

        private static void TestOut(IDbms dbms)
        {
            string name;
            name = "fn_essai_out";
            (var ret, var statusMessage) = dbms.ExecStatusMessage(name, new object[] { "World" });
            Console.WriteLine("{0} returned value: {1}, and status message: {2}", name, ret, statusMessage);
        }
    }
}
