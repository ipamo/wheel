﻿using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using Wheel.Commands;

namespace ConsoleApp
{

    class Essai3Command
    {
        public static void ConfigureCommand(ICommandBuilder cmd, IServiceCollection services)
        {
            services.AddSingleton<Essai3Command>();
            cmd.Description = "A command using services and raising a warning";
            var nOpt = cmd.Option("-n|--nb", "Number of ticks");
            cmd.Action = () =>
            {
                var o = cmd.Services.GetRequiredService<Essai3Command>();
                o.Run(nOpt.HasValue ? Convert.ToInt32(nOpt.Value) : 3);
            };
        }

        private readonly ILogger _logger;

        public Essai3Command(ILoggerFactory loggerFactory)
        {
            _logger = loggerFactory.CreateLogger<ILogger>();
        }

        public void Run(int ticks)
        {
            for (int i = 1; i <= ticks; i++)
            {
                _logger.LogWarning($"essai3: {i}");
                Thread.Sleep(500);
            }
        }
    }
}
