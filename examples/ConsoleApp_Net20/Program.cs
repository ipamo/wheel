﻿using System;
using Microsoft.Extensions.DependencyInjection;
using Wheel;
using Microsoft.Extensions.Logging;
using Wheel.Commands;
using Microsoft.Extensions.Configuration;

namespace ConsoleApp_Net20
{
    class Program
    {
        static int Main(string[] args)
            => CommandApplication.CreateDefaultBuilder()
                .UseStartup<Program>()
                .Build()
                .Run(args);

        public static void ConfigureCommands(ICommandBuilder root, ILoggerFactory loggerFactory, IConfiguration config)
        {
            root.DefaultCommand<Essai1Command>();

            root.Command("essai2", cmd =>
            {
                cmd.Description = "A command configured using actions and raising a warning";
                cmd.Action = () =>
                {
                    var logger = loggerFactory.CreateLogger<Program>();
                    Console.WriteLine("Hello world from essai2");
                    logger.LogWarning("Warning from essai2");
                    logger.LogInformation("Read configuration: " + config["Section:Key"]);
                };
            });

            root.Command<Essai3Command>();
        }
    }
}
