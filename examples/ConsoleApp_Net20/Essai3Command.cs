﻿using System;
using System.Collections.Generic;
using System.Text;
using Wheel.Commands;

namespace ConsoleApp_Net20
{
    [Command("essai-r", Description = "A command configured using attributes")]
    class Essai3Command
    {
        [CommandOption(Description = "Should we speak french ?")]
        public bool French { get; set; }

        public void Run(string name)
        {
            Console.WriteLine($"Hello world, {name}");
        }
    }
}
