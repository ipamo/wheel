﻿using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using System;
using System.Linq;
using Wheel;
using Wheel.Commands;

namespace HelloWorld
{
    /// <summary>
    /// Example of command explicitely defined with a ConfigureCommand.
    /// </summary>
    class HelloCommand
    {
        public void ConfigureCommand(ICommandBuilder cmd, ILogger<HelloCommand> logger)
        {
            cmd.Description = "A polite command.";
            var frenchOpt = cmd.Option("-f|--french", "Use French language.", noValue: true);
            var levelArg = cmd.Option("-l|--level", "Log level");
            var nameArg = cmd.Argument("[name]", "Your name.");

            cmd.Action = () =>
            {
                //var logger = cmd.Services.GetRequiredService<ILogger<HelloCommand>>();
                var level = levelArg.HasValue ? Enum.Parse<LogLevel>(levelArg.Value) : LogLevel.Information;
                SayHello(nameArg.Value ?? "world", frenchOpt.IsEnabled, logger, level);
            };
        }

        private static void SayHello(string name, bool french, ILogger logger, LogLevel level)
        {
            logger.Log(level, french ? "Bonjour, {0}." : "Hello, {0}.", name);
            logger.LogCritical("Test Critical");
            logger.LogError("Test Error");
            logger.LogWarning("Test Warning");
            logger.LogInformation("Test Information");
            logger.LogDebug("Test Debug");
            logger.LogTrace("Test Trace");
        }
    }
}
