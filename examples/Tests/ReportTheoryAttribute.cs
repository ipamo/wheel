﻿using Xunit;

namespace Wheel.Testing
{
    /// <summary>
    /// A xunit theory skipped if settings directive "Tests:SkipReport" is enabled.
    /// </summary>
    public class ReportTheoryAttribute : TheoryAttribute
    {
        public ReportTheoryAttribute()
            : base()
        {
            Skip = TestingInit.SkipReport;
        }
    }
}
