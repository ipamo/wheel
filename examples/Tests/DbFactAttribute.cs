﻿using Wheel.Data;
using Xunit;

namespace Wheel.Testing
{
    /// <summary>
    /// A xunit fact skipped if settings directive "Tests:SkipDb" is enabled.
    /// </summary>
    public class DbFactAttribute : FactAttribute
    {
        public DbFactAttribute(string cskey = null)
            : base()
        {
            Skip = TestingInit.SkipDb;
        }
    }
}
