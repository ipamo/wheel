﻿using Wheel.Data;
using Xunit;

namespace Wheel.Testing
{
    /// <summary>
    /// A xunit theory skipped if settings directive "Tests:SkipDb" is enabled.
    /// </summary>
    public class DbTheoryAttribute : TheoryAttribute
    {
        public DbTheoryAttribute(string cskey = null)
            : base()
        {
            Skip = TestingInit.SkipDb;
        }
    }
}
