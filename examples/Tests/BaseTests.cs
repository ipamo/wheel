﻿using Wheel.Data;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Xunit;
using Xunit.Abstractions;
using Wheel.Testing;
using Microsoft.EntityFrameworkCore;

namespace Wheel.Testing
{
    /// <summary>
    /// Base class for tests classes using <see cref="IFixture"/>.
    /// </summary>
    public abstract class BaseTests : IDisposable
    {
        protected readonly IFixture _fx;
        protected readonly IEnv _env;
        protected readonly IServiceProvider _services;
        protected readonly ITestOutputHelper _output;
        protected readonly TestReporter _reporter;

        public BaseTests(IFixture fx, ITestOutputHelper output)
        {
            _fx = fx;
            _env = fx.Env;
            _services = fx.Services;
            _output = output;
            _reporter = new TestReporter(_output);
            ((Env)_env).RegisterReporter(_reporter);
        }
        
        public virtual void Dispose()
        {
            ((Env)_env).UnregisterReporter(_reporter);
        }
    }
}
