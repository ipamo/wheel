﻿using System;
using Microsoft.Extensions.Configuration;
using Wheel.Data;
using System.Collections.Generic;
using System.Linq;
using System.Diagnostics;

namespace Wheel.Testing
{
    /// <summary>
    /// The goal of this class is is to determine whether tests with extended Fact and Theory
    /// attributes (such as <see cref="DbFactAttribute"/> and <see cref="DbTheoryAttribute"/>) will be skipped.
    /// <para>
    /// Note: discovery of tests with the extended attributes will fail in case of exception.
    /// If this happens, TestsInit will also fail and display the TypeInitializationException.
    /// </para>
    /// </summary>
    public static class TestingInit
    {
        private static IEnv _env;

        static TestingInit()
        {
            _env = new Env(environmentName: "Testing", applicationAssembly: null, disableLogging: true);
            Settings = _env.Settings;
            SetSkipDb();
            SetSkipReport();
        }

        public static IConfigurationRoot Settings { get; }
        public static IDbms Dbms { get; private set; }

        internal static string SkipDb { get; private set; }
        internal static string SkipReport { get; private set; }
        
        private static void SetSkipDb()
        {
            try
            {
                if (Settings.GetValue<bool>("Tests:SkipDb"))
                {
                    SkipDb = "Tests:SkipDb directive is set";
                    return;
                }

                Dbms = GetDbmsFromConfiguration(_env);
                
                using (var conn = Dbms.OpenConnection())
                {
                    var dbname = Dbms.GetDatabaseName(conn);
                    if (!(dbname.ToLowerInvariant().Contains("test") || dbname.ToLowerInvariant().EndsWith("_t")))
                    {
                        SkipDb = $"Database name \"{dbname}\" does not contain \"test\" or does not ends with \"_T\"";
                        return;
                    }
                }
            }
            catch (Exception e)
            {
                SkipDb = e.Message;
            }
        }

        private static void SetSkipReport()
        {
            if (Settings.GetValue<bool>("Tests:SkipReport"))
            {
                SkipReport = "Tests:SkipReport directive is set";
                return;
            }
            if (string.IsNullOrWhiteSpace(Settings["Smtp:Host"]))
            {
                SkipReport = "Smtp:Host directive is not set";
                return;
            }
            if (string.IsNullOrWhiteSpace(Settings["Report:To"]))
            {
                SkipReport = "Report:To directive is not set";
                return;
            }
        }

        /// <summary>
        /// Get the Dbms indicated in the 'Dbms:Type' configuration directive.
        /// </summary>
        private static IDbms GetDbmsFromConfiguration(IEnv env)
        {
            try
            {
                var config = env.Settings;

                if (string.IsNullOrWhiteSpace(config["Dbms:Type"]))
                    throw new InvalidOperationException("Directive Dbms:Type not set");

                var dbmsTypeFullName = config["Dbms:Type"].Trim();
                Type dbmsType = null;
                try
                {
                    dbmsType = Type.GetType(dbmsTypeFullName);
                }
                catch (Exception e)
                {
                    throw new InvalidOperationException($"Cannot find Dbms:Type \"{dbmsTypeFullName}\": {e.Message}");
                }

                if (dbmsType == null)
                    throw new InvalidOperationException($"Cannot find Dbms:Type \"{dbmsTypeFullName}\"");

                var constructors = dbmsType.GetConstructors();
                if (constructors.Length == 0)
                    throw new InvalidOperationException(string.Format("No constructor", dbmsType));
                else if (constructors.Length > 1)
                    throw new InvalidOperationException(string.Format("Several constructors", dbmsType));

                var availableParams = new Dictionary<Type, object>
                {
                    { typeof(IEnv), env }
                };

                var parameters = constructors[0].GetParameters();
                var parameterValues = new object[parameters.Length];
                var unknownParameters = new List<string>(parameterValues.Length);
                for (int i = 0; i < parameterValues.Length; i++)
                {
                    var type = parameters[i].ParameterType;
                    if (availableParams.ContainsKey(type))
                        parameterValues[i] = availableParams[type];
                    else
                        unknownParameters.Add(type.Name);
                }

                if (unknownParameters.Any())
                {
                    var types = unknownParameters.Count > 1 ? "types " + string.Join(", ", unknownParameters.ToArray()) : "type " + unknownParameters[0];
                    throw new InvalidOperationException(string.Format("Constructor parameter {1} not available", dbmsType, types));
                }

                return (IDbms)Activator.CreateInstance(dbmsType, parameterValues);
            }
            catch (Exception e)
            {
                throw new MessageException("Cannot instanciate SettingsDbmsSelector: " + e.Message);
            }
        }
    }
}
