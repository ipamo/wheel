﻿using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using WebApp.Models;
using Wheel.Data;

namespace WebApp.Data
{
    public class ApplicationDbContext : IdentityDbContext<User, Role, long>
    {
        protected readonly IDbms _dbms;
        private readonly DbContextHelper _helper;

        public DbSet<Component> Components { get; set; }
        public DbSet<Dict> Dicts { get; set; }

        public ApplicationDbContext(DbContextOptions options,
            IDbms dbms)
            : base(options)
        {
            _dbms = dbms;
            _helper = new DbContextHelper(dbms);
        }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);
            _helper.OnModelCreating(builder);
        }

        public override int SaveChanges(bool acceptAllChangesOnSuccess)
        {
            _helper.SaveChanges_AssignEdition(ChangeTracker);
            return base.SaveChanges(acceptAllChangesOnSuccess);
        }

        public override async Task<int> SaveChangesAsync(bool acceptAllChangesOnSuccess, CancellationToken cancellationToken = default(CancellationToken))
        {
            _helper.SaveChanges_AssignEdition(ChangeTracker);
            return await base.SaveChangesAsync(acceptAllChangesOnSuccess, cancellationToken);
        }
    }
}
