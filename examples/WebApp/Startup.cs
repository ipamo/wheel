﻿using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Hosting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebApp.Commands;
using WebApp.Data;
using WebApp.Models;
using Wheel.Commands;
using Wheel.Data;

namespace WebApp
{
    class Startup
    {
        private readonly IWebHostEnvironment _env;
        private readonly IConfiguration _config;

        public Startup(IWebHostEnvironment env, IConfiguration config)
        {
            _env = env;
            _config = config;
        }

        public void ConfigureServices(IServiceCollection services)
        {
            var connectionString = _config.GetConnectionString("Default");
            if (connectionString.Contains("Integrated Security"))
            {
                services.AddSingleton<IDbms, SqlServerDbms>();
                services.AddDbContext<ApplicationDbContext>(o => o.UseSqlServer(connectionString));
            }
            else
            {
                services.AddSingleton<IDbms, PostgreSqlDbms>();
                services.AddDbContext<ApplicationDbContext>(o => o.UseNpgsql(connectionString));
            }

            services.AddIdentity<User, Role>(options =>
            {
                // User settings
                options.User.AllowedUserNameCharacters = @"abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789-._@+\";
                options.User.RequireUniqueEmail = true;

                // Password settings
                options.Password.RequireDigit = false;
                options.Password.RequiredLength = 6;
                options.Password.RequireNonAlphanumeric = false;
                options.Password.RequireLowercase = false;
                options.Password.RequireUppercase = false;

                // Lockout settings
                options.Lockout.DefaultLockoutTimeSpan = TimeSpan.FromMinutes(30);
                options.Lockout.MaxFailedAccessAttempts = 5;
                options.Lockout.AllowedForNewUsers = true;
            })
                .AddEntityFrameworkStores<ApplicationDbContext>()
                .AddDefaultTokenProviders();

            if (_env.WebRootPath != null)
            {
                services.AddMvc();

                services.AddAuthentication();
            }
        }

        public void ConfigureCommands(ICommandBuilder root)
        {
            root.Command<HelloCommand>();
        }

        public void Configure(IApplicationBuilder app, ILogger<Startup> logger)
        {
            logger.LogCritical("Test Critical");
            logger.LogError("Test Error");
            logger.LogWarning("Test Warning");
            logger.LogInformation("Test Information");
            logger.LogDebug("Test Debug");
            logger.LogTrace("Test Trace");

            if (_env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseDatabaseErrorPage();
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
            }

            app.UseStaticFiles();

            app.UseAuthentication();
        }
    }
}
