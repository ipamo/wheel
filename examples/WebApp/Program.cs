﻿using System;
using Microsoft.Extensions.DependencyInjection;
using Wheel;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Identity;
using Wheel.Data;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using WebApp.Models;
using WebApp.Data;
using Wheel.Web;
using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.Configuration;
using Microsoft.AspNetCore;
using Microsoft.Extensions.Hosting;
using Wheel.Commands;
using Microsoft.AspNetCore.Hosting;

namespace WebApp
{
    class Program
    {
        static int Main(string[] args)
        {
            if (args.Length == 0)
            {
                Host.CreateDefaultBuilder(args)
                    .ConfigureWebHostDefaults(builder => builder.UseStartup<Startup>())
                    .ConfigureAppConfiguration(ConfigurationUtils.AddWheelToWebHost)
                    .ConfigureLogging(LoggingPlusUtils.AddWheelToWebHost)
                    .Build()
                    .Run();
                return ReturnCode.OK;
            }
            else
            {
                return CommandApplication.CreateDefaultBuilder()
                    .UseStartup<Startup>()
                    .Build()
                    .Run(args);
            }
        }
    }
}
