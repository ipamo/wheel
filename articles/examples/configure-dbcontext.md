Use IDbms to configure DbContext classes
========================================

## Define IDbms service

An @Wheel.Data.IDbms service can be registered in order to provide features adapted to the database management system used by the application.

In the application startup ConfigureServices method:

```c#
services.AddScoped<IDbms, PostgreSqlDbms>();
```

It is also possible to override a default Dbms implementation (PostgreSqlDbms, SqlServerDbms or OracleDbms) to use specific defaults, for example:

```c#
public class MyDbms : PostgreSqlDbms
{
	public MyDbms(IServiceProvider services)
		: base(services)
	{

	}

	public override int DefaultExecTimeout => 600;

	public override string InsertedAtColumn => "created_at";
	public override string InsertedOnColumn => "created_on";
	public override string InsertedByRefIdColumn => "created_by_user_id";
	public override string InsertedByComponentIdColumn => "created_by_component_id";
	
	// ...
}
```

This custom Dbms would be added in the startup ConfigureServices method like this:

```c#
services.AddScoped<IDbms, MyDbms>();
```


## Configure DbContext classes

To make your DbContext classes aware of IDbms features, they may be configured like this:

```c#
public class ApplicationDbContext : IdentityDbContext<User, Role, long>
{
	protected readonly IDbms _dbms;

	public DbSet<Component> Components { get; set; }
	public DbSet<Dict> Dicts { get; set; }

	public ApplicationDbContext(DbContextOptions options, IDbms dbms)
		: base(options)
	{
		_dbms = dbms;
	}

	protected override void OnModelCreating(ModelBuilder builder)
	{
		base.OnModelCreating(builder);
		builder.OnModelCreatingDbms(_dbms);
	}

	public override int SaveChanges(bool acceptAllChangesOnSuccess)
	{
		this.SaveChangesDbms(_dbms);
		return base.SaveChanges(acceptAllChangesOnSuccess);
	}

	public override async Task<int> SaveChangesAsync(bool acceptAllChangesOnSuccess, CancellationToken cancellationToken = default(CancellationToken))
	{
		this.SaveChangesDbms(_dbms);
		return await base.SaveChangesAsync(acceptAllChangesOnSuccess, cancellationToken);
	}
}
```


## Use 'dotnet ef' with the DbContext class

To allow 'dotnet ef' to work with your DbContext class, a class implementing the `IDesignTimeDbContextFactory<ApplicationDbContext>` interface must exist in the assembly in order to indicate how to create an instance of the DbContext.

Example:

```c#
public class DbContextFactory : IDesignTimeDbContextFactory<ApplicationDbContext>
{
	public ApplicationDbContext CreateDbContext(string[] args)
	{
		var services = CommandApplication.CreateDefaultBuilder()
			.UseStartup<Startup>()
			.Build()
			.Services;
		
		return services.GetRequiredService<ApplicationDbContext>();
	}
}
```
