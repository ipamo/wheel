﻿using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using System.Text;

namespace Wheel
{
    /// <summary>
    /// Helper methods for the configuration system.
    /// </summary>
    public static class ConfigurationUtils
    {
        /// <summary>
        /// Add appsettings.local.json and appsettings.{env}.local.json to the default web host.
        /// </summary>
        public static void AddWheelToWebHost(HostBuilderContext context, IConfigurationBuilder builder)
        {
            var env = context.HostingEnvironment;
            builder.AddJsonFile(Path.Combine(env.ContentRootPath, "appsettings.local.json"), optional: true, reloadOnChange: true);
            builder.AddJsonFile(Path.Combine(env.ContentRootPath, $"appsettings.{env.EnvironmentName}.local.json"), optional: true, reloadOnChange: true);
        }

        /// <summary>
        /// Configure an empty app configuration infrastructure like it would be done by aspnetcore default web host,
        /// and add <see cref="AddWheelToWebHost"/> features.
        /// </summary>
        internal static void WheelConfigure(HostBuilderContext context, IConfigurationBuilder builder)
        {
            var env = context.HostingEnvironment;

            // See https://github.com/aspnet/MetaPackages/blob/release/2.1/src/Microsoft.AspNetCore/WebHost.cs
            builder.AddJsonFile(Path.Combine(env.ContentRootPath, "appsettings.json"), optional: true, reloadOnChange: true);
            builder.AddJsonFile(Path.Combine(env.ContentRootPath, $"appsettings.{env.EnvironmentName}.json"), optional: true, reloadOnChange: true);
            // See WebHostConfigure
            AddWheelToWebHost(context, builder);

#if !NET20
            if (env.IsDevelopment() || env.IsEnvironment("Testing"))
            {
                var appAssembly = Assembly.Load(new AssemblyName(env.ApplicationName));
                if (appAssembly != null)
                {
                    builder.AddUserSecrets(appAssembly, optional: true);
                }
            }

            builder.AddEnvironmentVariables();
#endif
        }
    }
}
