﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Wheel
{
    public static class ServiceCollectionUtils
    {
        /// <summary>
        /// Get service instance that was previously added in the service collection.
        /// </summary>
        public static TService GetInstance<TService>(this IServiceCollection services)
        {
            var sd = services.FirstOrDefault(x => x.ServiceType == typeof(TService));
            if (sd == null)
                throw new InvalidOperationException($"{typeof(TService)} not found in service collection");

            if (sd.ImplementationInstance == null)
                throw new InvalidOperationException($"ImplementationInstance is null for {typeof(TService)} service descriptor");

            return (TService)sd.ImplementationInstance;
        }

#if !NET20
        public static void BindAndValidateOptions<TOptions>(this IServiceCollection services, IConfiguration section)
            where TOptions : class
        {
            services.AddOptions<TOptions>()
                .Bind(section)
                .ValidateDataAnnotations();
        }
#endif
    }
}
