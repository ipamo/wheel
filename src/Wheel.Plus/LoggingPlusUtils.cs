﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using LogLevel = Microsoft.Extensions.Logging.LogLevel;
using System.IO;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Hosting;
using NLog.Extensions.Logging;

namespace Wheel
{
    /// <summary>
    /// Helper methods for the logging system.
    /// </summary>
    public static class LoggingPlusUtils
    {
        /// <summary>
        /// Convert a <see cref="Microsoft.Extensions.Logging.LogLevel" /> into a NLog level.
        /// </summary>
        public static NLog.LogLevel ToNLogLevel(LogLevel level)
        {
            if (level == LogLevel.Critical)
                return NLog.LogLevel.Fatal;
            if (level == LogLevel.Error)
                return NLog.LogLevel.Error;
            if (level == LogLevel.Warning)
                return NLog.LogLevel.Warn;
            if (level == LogLevel.Debug)
                return NLog.LogLevel.Debug;
            if (level == LogLevel.Trace)
                return NLog.LogLevel.Trace;
            return NLog.LogLevel.Info;
        }

        /// <summary>
        /// Convert a NLog level into a <see cref="Microsoft.Extensions.Logging.LogLevel" />.
        /// </summary>
        public static LogLevel ToLoggingLevel(NLog.LogLevel nlogLevel)
        {
            if (nlogLevel == NLog.LogLevel.Fatal)
                return LogLevel.Critical;
            if (nlogLevel == NLog.LogLevel.Error)
                return LogLevel.Error;
            if (nlogLevel == NLog.LogLevel.Warn)
                return LogLevel.Warning;
            if (nlogLevel == NLog.LogLevel.Debug)
                return LogLevel.Debug;
            if (nlogLevel == NLog.LogLevel.Trace)
                return LogLevel.Trace;
            return LogLevel.Information;
        }

        /// <summary>
        /// Write NLog configuration for debug purpose.
        /// </summary>
        public static void DebugNLogConfig(NLog.Config.LoggingConfiguration config = null, TextWriter writer = null)
        {
            if (writer == null)
                writer = Console.Out;
            if (config == null)
                config = NLog.LogManager.Configuration;

            var logger = NLog.LogManager.GetLogger("Essai");
            writer.WriteLine("Level status for 'Essai' logger:");
            writer.WriteLine("- Fatal: {0}", logger.IsFatalEnabled);
            writer.WriteLine("- Error: {0}", logger.IsErrorEnabled);
            writer.WriteLine("- Warn: {0}", logger.IsWarnEnabled);
            writer.WriteLine("- Info: {0}", logger.IsInfoEnabled);
            writer.WriteLine("- Debug: {0}", logger.IsDebugEnabled);
            writer.WriteLine("- Trace: {0}", logger.IsTraceEnabled);
            writer.Flush();

            logger.Log(NLog.LogLevel.Fatal, "Essai Fatal");
            logger.Log(NLog.LogLevel.Error, "Essai Error");
            logger.Log(NLog.LogLevel.Warn, "Essai Warn");
            logger.Log(NLog.LogLevel.Info, "Essai Info");
            logger.Log(NLog.LogLevel.Debug, "Essai Debug");
            logger.Log(NLog.LogLevel.Trace, "Essai Trace");
            NLog.LogManager.Flush();

            writer.WriteLine(config.GetType());
            writer.WriteLine($"{config.AllTargets.Count} target(s)");
            foreach (var t in config.AllTargets)
            {
                writer.Write($"- {t.GetType()}: Name={t.Name}");
                if (t is global::NLog.Targets.ConsoleTarget)
                {
                    var cT = (global::NLog.Targets.ConsoleTarget)t;
                    writer.Write($", Layout={cT.Layout}");
                }
                else if (t is global::NLog.Targets.ColoredConsoleTarget)
                {
                    var cT = (global::NLog.Targets.ColoredConsoleTarget)t;
                    writer.Write($", Layout={cT.Layout}");
                }
                else if (t is global::NLog.Targets.FileTarget)
                {
                    var fT = (global::NLog.Targets.FileTarget)t;
                    writer.Write($", FileName={fT.FileName}, Layout={fT.Layout}");
                }
                writer.WriteLine();
            }
            writer.WriteLine($"{config.LoggingRules.Count} rule(s)");
            foreach (var r in config.LoggingRules)
            {
                writer.WriteLine($"- {r.GetType()}: LoggerNamePattern={r.LoggerNamePattern}, Final={r.Final}, Levels={string.Join("|", r.Levels.Select(s => s.ToString()).ToArray())}, Targets={string.Join("|", r.Targets.Select(t => t.Name).ToArray())}");
            }
        }

        /// <summary>
        /// Configure NLog using the first of the given <paramref name="configFiles"/> that exists.
        /// </summary>
        /// <param name="variables">(not used if .NET 2.0) Variables to pass to the config file (variable <c>key</c> is available in the nlog.config file using syntax <c>${var:key}</c>).</param>
        /// <param name="configFiles">nlog.config files.</param>
        /// <returns>The config file used, <c>null</c> if none existed.</returns>
        public static string ConfigureNLog(
            Dictionary<string, string> variables, // not used if .NET 2.0
            params string[] configFiles
            )
        {
            if (configFiles == null)
                throw new ArgumentNullException(nameof(configFiles));
            if (!configFiles.Any())
                throw new ArgumentException($"{nameof(configFiles)} cannot be empty", nameof(configFiles));

            foreach (var configFile in configFiles)
            {
                if (File.Exists(configFile))
                {
                    // Do the configuration
                    var config = new NLog.Config.XmlLoggingConfiguration(configFile, ignoreErrors: false);
#if !NET20
                    if (variables != null)
                    {
                        foreach (var entry in variables)
                            config.Variables[entry.Key] = entry.Value;
                    }
#endif
                    NLog.LogManager.Configuration = config;
                    return configFile;
                }
            }

            // NLog not configured
            return null;
        }


        /// <summary>
        /// Configure NLog using the first of the given <paramref name="configFiles"/> that exists.
        /// </summary>
        /// <param name="configFiles">nlog.config files.</param>
        /// <returns>The config file used, <c>null</c> if none existed.</returns>
        public static string ConfigureNLog(params string[] configFiles)
            => ConfigureNLog(null, configFiles);

        private static string[] NLogConfigFiles(IHostEnvironment env) => new string[]
        {
            Path.Combine(env.ContentRootPath, $"nlog.{env.EnvironmentName}.local.config"),
            Path.Combine(env.ContentRootPath, $"nlog.{env.EnvironmentName}.config"),
            Path.Combine(env.ContentRootPath, $"nlog.local.config"),
            Path.Combine(env.ContentRootPath, $"nlog.config")
        };

#if NET20

        /// <summary>
        /// Configure an empty logging infrastructure like it would be done by aspnetcore default web host,
        /// and add <c>AddWheelToWebHost</c> features.
        /// </summary>
        internal static void WheelConfigure(HostBuilderContext context, ILoggerFactory factory)
        {
            var nlogConfig = ConfigureNLog(NLogConfigFiles(context.HostingEnvironment));
            if (nlogConfig != null)
            {
                factory.AddNLog();
            }
            else
            {
                Console.Error.WriteLine("Logging not configured (missing file nlog.config or a derivative)");
                factory.AddConsole(LogLevel.Information);
            }
        }

#else
        /// <summary>
        /// Add NLog to the default web host, if file nlog.config (or a derivative) exist.
        /// </summary>
        public static void AddWheelToWebHost(HostBuilderContext context, ILoggingBuilder logging)
        {
            var nlogConfig = ConfigureNLog(NLogConfigFiles(context.HostingEnvironment));
            if (nlogConfig != null)
            {
                // We need to clear what is done in WebHost.CreateDefaultBuilder (see: https://github.com/aspnet/MetaPackages/blob/release/2.1/src/Microsoft.AspNetCore/WebHost.cs)
                logging.ClearProviders();
                logging.SetMinimumLevel(LogLevel.Trace);
                logging.AddNLog();
            }
        }

        /// <summary>
        /// Configure an empty logging infrastructure like it would be done by aspnetcore default web host,
        /// and add <see cref="AddWheelToWebHost"/> features.
        /// </summary>
        internal static void WheelConfigure(HostBuilderContext context, ILoggingBuilder logging)
        {
            // Kept even with NLog: minimal log levels from the configuration file applies
            logging.AddConfiguration(context.Configuration.GetSection("Logging"));

            var nlogConfig = ConfigureNLog(NLogConfigFiles(context.HostingEnvironment));
            if (nlogConfig != null)
            {
                logging.ClearProviders();
                logging.SetMinimumLevel(LogLevel.Trace);
                logging.AddNLog();
            }
            else
            {
                // We do the same as WebHost.CreateDefaultBuilder. See: https://github.com/aspnet/MetaPackages/blob/release/2.1/src/Microsoft.AspNetCore/WebHost.cs
                logging.AddConsole();
                logging.AddDebug();
            }
        }
#endif
    }
}
