﻿using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Net;
using System.Threading;

namespace Wheel.Internal
{
    /// <summary>
    /// Implementation of <see cref="IReporter"/> that is used as an addition logging provider.
    /// </summary>
    /// <remarks>
    /// If, for a given thread, there is no registered reporter, the last used reporter will be used.
    /// It is necessary in case the registration of reporters is forgotten in multithreaded applications (which is probable).
    /// It is a risk in case multiple commands are run in parallel... but this should not happen, except in very specific scenarios
    /// (in this case, we expect the program to explicitly do reporter registration...)
    /// </remarks>
    internal class ReporterManager : IReporterManager, ILogger
    {
        private readonly Dictionary<int, IReporter> _reporters;
        private IReporter lastReporter;

        public ReporterManager()
        {
            _reporters = new Dictionary<int, IReporter>();
        }

        public void Log<TState>(LogLevel logLevel, EventId eventId, TState state, Exception exception, Func<TState, Exception, string> formatter)
        {
            lock (_reporters)
            {
                var reporter = Reporter;
                if (reporter != null)
                    lastReporter = reporter;
                else
                    reporter = lastReporter;

                if (reporter == null)
                    return;
                
                if (formatter == null)
                    throw new ArgumentNullException(nameof(formatter));

                reporter.Log(logLevel, eventId, state, exception, formatter);
            }
        }

        public bool IsEnabled(LogLevel logLevel)
        {
            return true;
        }

        public IDisposable BeginScope<TState>(TState state)
        {
            return null;
        }
        
        public IReporter Reporter
        {
            get
            {
                lock (_reporters)
                {
                    var threadId = Thread.CurrentThread.ManagedThreadId;
                    if (_reporters.ContainsKey(threadId))
                        return _reporters[threadId];
                    else
                        return null;
                }
            } 
        }

        public void Register(IReporter reporter)
        {
            lock (_reporters)
            {
                if (reporter == null)
                    throw new ArgumentNullException(nameof(reporter));
                
                var threadId = Thread.CurrentThread.ManagedThreadId;
                _reporters[threadId] = reporter;
            }
        }

        public void Unregister(bool allThreads = false)
        {
            lock (_reporters)
            {
                var threadId = Thread.CurrentThread.ManagedThreadId;
                if (!_reporters.TryGetValue(threadId, out var reporter))
                    return;
                _reporters.Remove(threadId);

                if (allThreads)
                {
                    var threadIds = _reporters.Where(e => e.Value == reporter).Select(e => e.Key).ToArray();
                    foreach (var id in threadIds)
                    {
                        _reporters.Remove(id);
                    }
                }
            }
        }
    }
}
