﻿#if !NET20
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Wheel
{
    /// <summary>
    /// Service for sending emails.
    /// </summary>
    public interface IEmailSender
    {
        Task SendEmailAsync(
            string to,
            string subject,
            string textMessage,
            string htmlBodyContent,
            List<string> attachments = null,
            bool highImportance = false);

        Task SendTextEmailAsync(
            string to,
            string subject,
            string message,
            string attachment = null,
            bool highImportance = false);

        Task SendTextEmailAsync(
            string to,
            string subject,
            string message,
            List<string> attachments,
            bool highImportance = false);

        Task SendHtmlEmailAsync(
            string to,
            string subject,
            string bodyContent,
            string attachment = null,
            bool highImportance = false);

        Task SendHtmlEmailAsync(
            string to,
            string subject,
            string bodyContent,
            List<string> attachments,
            bool highImportance = false);
    }
}
#endif
