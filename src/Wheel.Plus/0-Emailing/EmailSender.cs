﻿#if !NET20
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Extensions.Options;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using MimeKit;
using MimeKit.Text;
using MailKit.Net.Smtp;
using MailKit.Security;
using System.Text;
using System.IO;

namespace Wheel
{
    /// <summary>
    /// An implementation of <see cref="IEmailSender"/> based on MailKit.
    /// </summary>
    public class EmailSender : IEmailSender
    {
        private ILogger _logger;
        private SmtpOptions _options;
        private ICssInliner _cssInliner;

        public EmailSender(ILogger<EmailSender> logger,
            IOptions<SmtpOptions> optionsAccessor,
            ICssInliner cssInliner)
        {
            _logger = logger;
            _options = optionsAccessor.Value;
            _cssInliner = cssInliner;
        }
        
        private string BuildFullHtml(string bodyContent)
        {
            var html = $@"<!DOCTYPE html PUBLIC ""-//W3C//DTD XHTML 1.0 Strict//EN"" ""http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd"">
<html xmlns=""http://www.w3.org/1999/xhtml"">
<head>
	<meta http-equiv=""Content-Type"" content=""text/html;charset=utf-8"" />
    <meta name=""viewport"" content=""width=device-width""/>
</head>
<body>
{bodyContent}
</body>
</html>";

            if (_cssInliner != null)
            {
                return _cssInliner.Inline(html) ?? html;
            }
            else
            {
                return html;
            }
        }


        public async Task SendEmailAsync(
            string to,
            string subject,
            string textMessage,
            string htmlBodyContent,
            List<string> attachments = null,
            bool highImportance = false)
        {
            if (string.IsNullOrWhiteSpace(to))
                throw new ArgumentException($"Cannot send email: no recipient", "to");

            if (string.IsNullOrWhiteSpace(_options.Host))
                throw new EmailSenderException("SMTP host not configured", to);

            if (!string.IsNullOrWhiteSpace(_options.User) && string.IsNullOrWhiteSpace(_options.Password))
                throw new EmailSenderException("SMTP password not configured", to);
            
            var toSplit = to.Split(new char[] { ',', ';', '|' }).Select(e => e.Trim()).Where(e => e != "");

            try
            {
                var mime = new MimeMessage();
                mime.From.Add(MailboxAddress.Parse(_options.From));
                if (!string.IsNullOrWhiteSpace(_options.ReplyTo))
                    mime.ReplyTo.Add(MailboxAddress.Parse(_options.ReplyTo));
                foreach (var email in toSplit)
                    mime.To.Add(MailboxAddress.Parse(email));
                mime.Subject = subject;

                var body = new BodyBuilder();
                if (htmlBodyContent != null)
                    body.HtmlBody = BuildFullHtml(htmlBodyContent);
                if (textMessage != null)
                    body.TextBody = textMessage;

                if (attachments != null)
                {
                    foreach (var attachment in attachments)
                        body.Attachments.Add(attachment);
                }

                mime.Body = body.ToMessageBody();

                if (highImportance)
                {
                    mime.Importance = MessageImportance.High;
                    mime.XPriority = XMessagePriority.Highest;
                    mime.Headers["X-MSMail-Priority"] = "High";
                }

                using (var client = new SmtpClient())
                {
                    var security = _options.Ssl ? SecureSocketOptions.Auto : SecureSocketOptions.None;
                    await client.ConnectAsync(_options.Host, _options.Port, security);

                    if (!string.IsNullOrWhiteSpace(_options.User))
                        await client.AuthenticateAsync(_options.User, _options.Password);

                    await client.SendAsync(mime);
                    await client.DisconnectAsync(true);
                }
            }
            catch (Exception e)
            {
                while (e.InnerException != null)
                    e = e.InnerException;
                throw new EmailSenderException(e.Message, to);
            }
        }

        public async Task SendTextEmailAsync(
            string to,
            string subject,
            string message,
            string attachment = null,
            bool highImportance = false)
                =>
                await SendEmailAsync(to, subject, textMessage: message, htmlBodyContent: null, attachments: attachment != null ? new List <string> { attachment } : null, highImportance: highImportance);

        public async Task SendTextEmailAsync(
            string to,
            string subject,
            string message,
            List<string> attachments,
            bool highImportance = false)
                =>
                await SendEmailAsync(to, subject, textMessage: message, htmlBodyContent: null, attachments: attachments, highImportance: highImportance);

        public async Task SendHtmlEmailAsync(
            string to,
            string subject,
            string bodyContent,
            string attachment = null,
            bool highImportance = false)
                =>
                await SendEmailAsync(to, subject, textMessage: null, htmlBodyContent: bodyContent, attachments: attachment != null ? new List<string> { attachment } : null, highImportance: highImportance);

        public async Task SendHtmlEmailAsync(
            string to,
            string subject,
            string bodyContent,
            List<string> attachments,
            bool highImportance = false)
                =>
                await SendEmailAsync(to, subject, textMessage: null, htmlBodyContent: bodyContent, attachments: attachments, highImportance: highImportance);
    }
}
#endif
