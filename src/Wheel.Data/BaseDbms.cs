﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Text;
using System.Data.Common;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.EntityFrameworkCore;
using System.Data;
using System.Reflection;

namespace Wheel.Data
{
    /// <summary>
    /// Default implementation of DBMS features.
    /// </summary>
    public abstract class BaseDbms : IDbms
    {
        protected IConfiguration _config;
        protected ILoggerFactory _loggerFactory;
        protected ILogger _logger;

        public BaseDbms(IConfiguration config,
            ILoggerFactory loggerFactory,
            string defaultCskey = null)
        {
            _config = config;
            _loggerFactory = loggerFactory;
            _logger = _loggerFactory.CreateLogger(GetType());
            DefaultCskey = defaultCskey ?? "Default";
        }

        public abstract Type DbConnectionType { get; }

        public string DefaultCskey { get; }

        public virtual int DefaultExecTimeout => 60;

        public abstract string DefaultSchema { get; }

        /// <summary>
        /// Get a base name for the table, from the entity type namespace, using capitalized format.
        /// </summary>
        protected virtual string GetBaseTableName(Type entityType)
        {
            if (entityType.FullName.StartsWith("Microsoft.AspNetCore.Identity.Identity"))
            {
                var startsAfter = "Microsoft.AspNetCore.Identity.Identity".Length;
                var stopsAt = entityType.FullName.IndexOf("`");
                if (stopsAt > startsAfter)
                {
                    // Retire le début du nom ainsi que la fin à partir de "`".
                    return entityType.FullName.Substring(startsAfter, entityType.FullName.IndexOf("`") - startsAfter);
                }
            }

            return entityType.Name;
        }


        public virtual string GetTableName(Type entityType) => GetBaseTableName(entityType);

        public virtual string GetViewName(Type entityType) => GetTableName(entityType);

        public virtual string GetSchemaName(Type entityType) => DefaultSchema;

        public virtual string GetColumnName(Type entityType, string propertyName) => propertyName;

        public virtual string GetColumnType(Type entityType, string propertyName, Type propertyType) => null;

        public virtual string InsertedAtProperty => null;
        public virtual string[] InsertedAtAlternatives => null;
        public virtual string InsertedByIdProperty => null;
        public virtual string InsertedThroughIdProperty => null;

        public virtual string UpdatedAtProperty => null;
        public virtual string[] UpdatedAtAlternatives => null;
        public virtual string UpdatedByIdProperty => null;
        public virtual string UpdatedThroughIdProperty => null;

        public virtual bool AssignUpdatedPropertiesAtInsertion => false;
        public virtual Type ByIdType => typeof(long);
        public virtual Type ByIdRelatedEntity => null;
        public virtual Type ThroughIdType => typeof(long);

        public virtual string[] DateOnlyProperties => null;
        
        public abstract string DateOnlyNowSql { get; }
        public abstract string DateTimeNowSql { get; }

        public virtual string DateOnlySqlType => null;
        public virtual string DateTimeSqlType => null;
        public virtual string StringSqlType(int maxLength = -1) => null;
        public virtual string DecimalSqlType(int precision, int scale) => $"decimal({precision},{scale})";

        protected abstract char IdentifierProtectBegin { get; }
        protected abstract char IdentifierProtectEnd { get; }


        public string GetConnectionString(string cskey = null)
        {
            cskey = cskey ?? DefaultCskey;
            var connectionString = _config.GetConnectionString(cskey);
            if (string.IsNullOrWhiteSpace(connectionString))
            {
                throw new KeyNotFoundException($"ConnectionStrings:{cskey} directive not set");
            }
            return connectionString.Trim();
        }

        public abstract DbConnection InstanciateConnection(string connectionString);

        public DbConnection OpenConnection(string cskey = null)
        {
            var conn = InstanciateConnection(GetConnectionString(cskey));
            conn.Open();
            return conn;
        }

        private Dictionary<string, DbConnection> _connections;

        public DbConnection RetrieveConnection(string cskey = null, string oid = null)
        {
            if (cskey == null)
                cskey = DefaultCskey;
            if (oid == null)
                oid = cskey;

            if (_connections == null)
                _connections = new Dictionary<string, DbConnection>();

            DbConnection connection;
            if (!_connections.ContainsKey(cskey))
            {
                connection = OpenConnection(cskey);
                _connections[oid] = connection;
            }
            else
            {
                connection = _connections[oid];
            }
            return connection;
        }

        public DbConnection DefaultConnection
        {
            get
            {
                var conn = RetrieveConnection();
                if (conn.State != ConnectionState.Open)
                    conn.Open();
                return conn;
            }
        }

        public abstract string GetDatabaseName(DbConnection connection);

        public abstract short GetComponentId(string name);
        
        public short GetComponentId(Type type) => GetComponentId(type.FullName);
        
        public short GetComponentId<T>() => GetComponentId(typeof(T).FullName);


        protected abstract void SetDictValue(string key, string value);
        public abstract string GetDictValue(string key);
        public abstract void RemoveDictValue(string key);


        public void SetDictValue(string key, object value)
        {
            if (value == null)
                SetDictValue(key, "");
            else
                SetDictValue(key, value.ToString());
        }

        public TValue GetDictValue<TValue>(string key)
        {
            var value = GetDictValue(key);
            if (value == null)
                throw new KeyNotFoundException($"Dict key \"{key}\" does not exist");
            return (TValue)Convert.ChangeType(value, typeof(TValue));
        }
        
        public string SanitizeIdentifier(string identifier)
        {
            return ParseIdentifier(identifier).Sanitize();
        }

        public string SanitizeIdentifier(DbIdentifier identifier)
        {
            return identifier.Sanitize();
        }
        
        public DbIdentifier ParseIdentifier(string identifier)
        {
            return new DbIdentifier(protectBegin: IdentifierProtectBegin, protectEnd: IdentifierProtectEnd, defaultSchema: DefaultSchema)
                .Parse(identifier);
        }

        protected virtual string PositionParameterNamePrefix => "@";

        protected virtual object ConvertParameterValue(object value)
        {
            if (value == null)
                return null;

            var type = value.GetType();
            var underlyingType = Nullable.GetUnderlyingType(type);
            if (underlyingType != null)
            {
                type = underlyingType;
                value = Convert.ChangeType(value, underlyingType);
            }

            if (type == typeof(bool))
                return (bool)value ? 1 : 0;
            
            return value;
        }

        public DbParameterCollection AddParameters(DbCommand cmd, params object[] parameters)
        {
            if (cmd == null)
                throw new ArgumentNullException(nameof(cmd));
            if (parameters == null)
                throw new ArgumentNullException(nameof(parameters));
            
            for (int i = 1; i <= parameters.Length; i++)
            {
                var p = cmd.CreateParameter();
                p.ParameterName = PositionParameterNamePrefix + i;
                p.Value = ConvertParameterValue(parameters[i - 1]);
                cmd.Parameters.Add(p);
            }

            return cmd.Parameters;
        }

        public DbParameterCollection AddParameters(DbCommand cmd, Dictionary<string, object> parameters)
        {
            if (cmd == null)
                throw new ArgumentNullException(nameof(cmd));
            if (parameters == null)
                throw new ArgumentNullException(nameof(parameters));

            foreach (var e in parameters)
            {
                var p = cmd.CreateParameter();
                p.ParameterName = e.Key;
                p.Value = ConvertParameterValue(e.Value);
                cmd.Parameters.Add(p);
            }

            return cmd.Parameters;
        }

        public void NonQuery(string sql, params object[] parameters)
        {
            using (var cmd = DefaultConnection.CreateCommand())
            {
                cmd.CommandText = sql;
                if (parameters != null)
                    AddParameters(cmd, parameters);
                cmd.ExecuteNonQuery();
            }
        }
        
        public void NonQuery(string sql, Dictionary<string, object> parameters)
        {
            using (var cmd = DefaultConnection.CreateCommand())
            {
                cmd.CommandText = sql;
                if (parameters != null)
                    AddParameters(cmd, parameters);
                cmd.ExecuteNonQuery();
            }
        }
        
        public object QueryScalar(string sql, params object[] parameters)
        {
            using (var cmd = DefaultConnection.CreateCommand())
            {
                cmd.CommandText = sql;
                if (parameters != null)
                    AddParameters(cmd, parameters);
                return cmd.ExecuteScalar();
            }
        }
        
        public object QueryScalar(string sql, Dictionary<string, object> parameters)
        {
            using (var cmd = DefaultConnection.CreateCommand())
            {
                cmd.CommandText = sql;
                if (parameters != null)
                    AddParameters(cmd, parameters);
                return cmd.ExecuteScalar();
            }
        }

        public string QueryString(string sql, params object[] parameters)
        {
            var scalar = QueryScalar(sql, parameters);
            if (scalar == null || scalar == DBNull.Value)
                return null;
            return Convert.ToString(scalar);
        }

        public string QueryString(string sql, Dictionary<string, object> parameters)
        {
            var scalar = QueryScalar(sql, parameters);
            if (scalar == null || scalar == DBNull.Value)
                return null;
            return Convert.ToString(scalar);
        }

        public long? QueryLong(string sql, params object[] parameters)
        {
            var scalar = QueryScalar(sql, parameters);
            if (scalar == null || scalar == DBNull.Value)
                return null;
            return Convert.ToInt64(scalar);
        }

        public long? QueryLong(string sql, Dictionary<string, object> parameters)
        {
            var scalar = QueryScalar(sql, parameters);
            if (scalar == null || scalar == DBNull.Value)
                return null;
            return Convert.ToInt64(scalar);
        }

        public virtual int Exec(string procedure, object[] args, int timeout = 60, int? ifNoReturn = null, ILogger logger = null, string logPrefix = null, bool catchExceptions = false)
        {
            var identifier = ParseIdentifier(procedure);
            if (logger == null)
                logger = _loggerFactory.CreateLogger(identifier.ToString());
            var connectionString = GetConnectionString(identifier.Catalog ?? DefaultCskey);
            using (var connection = InstanciateConnection(connectionString))
            {
                var prepared = PrepareExec(connection, logger, logPrefix, catchExceptions);
                var namedArgs = GetNamedArgs(prepared, identifier, args);
                return Exec(prepared, identifier, namedArgs, timeout, ifNoReturn, withStatusMessage: false).returnCode;
            }
        }

        public virtual int Exec(string procedure, Dictionary<string, object> args = null, int timeout = 60, int? ifNoReturn = null, ILogger logger = null, string logPrefix = null, bool catchExceptions = false)
        {
            var identifier = ParseIdentifier(procedure);
            if (logger == null)
                logger = _loggerFactory.CreateLogger(identifier.ToString());
            var connectionString = GetConnectionString(identifier.Catalog ?? DefaultCskey);
            using (var connection = InstanciateConnection(connectionString))
            {
                var prepared = PrepareExec(connection, logger, logPrefix, catchExceptions);
                return Exec(prepared, identifier, args, timeout, ifNoReturn, withStatusMessage: false).returnCode;
            }
        }

        public virtual int Exec(IPreparedExec prepared, string procedure, object[] args, int timeout = -1, int? ifNoReturn = null)
        {
            var identifier = ParseIdentifier(procedure);
            var namedArgs = GetNamedArgs(prepared, identifier, args);
            return Exec(prepared, identifier, namedArgs, timeout, ifNoReturn, withStatusMessage: false).returnCode;
        }

        public virtual int Exec(IPreparedExec prepared, string procedure, Dictionary<string, object> args = null, int timeout = -1, int? ifNoReturn = null)
        {
            return Exec(prepared, ParseIdentifier(procedure), args, timeout, ifNoReturn, withStatusMessage: false).returnCode;
        }

        public virtual (int returnCode, string statusMessage) ExecStatusMessage(string procedure, object[] args, int timeout = 60, int? ifNoReturn = null, ILogger logger = null, string logPrefix = null, bool catchExceptions = false)
        {
            var identifier = ParseIdentifier(procedure);
            if (logger == null)
                logger = _loggerFactory.CreateLogger(identifier.ToString());
            var connectionString = GetConnectionString(identifier.Catalog ?? DefaultCskey);
            using (var connection = InstanciateConnection(connectionString))
            {
                var prepared = PrepareExec(connection, logger, logPrefix, catchExceptions);
                var namedArgs = GetNamedArgs(prepared, identifier, args);
                return Exec(prepared, identifier, namedArgs, timeout, ifNoReturn, withStatusMessage: true);
            }
        }

        public virtual (int returnCode, string statusMessage) ExecStatusMessage(string procedure, Dictionary<string, object> args = null, int timeout = 60, int? ifNoReturn = null, ILogger logger = null, string logPrefix = null, bool catchExceptions = false)
        {
            var identifier = ParseIdentifier(procedure);
            if (logger == null)
                logger = _loggerFactory.CreateLogger(identifier.ToString());
            var connectionString = GetConnectionString(identifier.Catalog ?? DefaultCskey);
            using (var connection = InstanciateConnection(connectionString))
            {
                var prepared = PrepareExec(connection, logger, logPrefix, catchExceptions);
                return Exec(prepared, identifier, args, timeout, ifNoReturn, withStatusMessage: true);
            }
        }

        public virtual (int returnCode, string statusMessage) ExecStatusMessage(IPreparedExec prepared, string procedure, object[] args, int timeout = -1, int? ifNoReturn = null)
        {
            var identifier = ParseIdentifier(procedure);
            var namedArgs = GetNamedArgs(prepared, identifier, args);
            return Exec(prepared, identifier, namedArgs, timeout, ifNoReturn, withStatusMessage: true);
        }

        public virtual (int returnCode, string statusMessage) ExecStatusMessage(IPreparedExec prepared, string procedure, Dictionary<string, object> args = null, int timeout = -1, int? ifNoReturn = null)
        {
            return Exec(prepared, ParseIdentifier(procedure), args, timeout, ifNoReturn, withStatusMessage: true);
        }

        protected abstract Dictionary<string, object> GetNamedArgs(IPreparedExec prepared, DbIdentifier procedure, object[] args);

        protected abstract (int returnCode, string statusMessage) Exec(IPreparedExec prepared, DbIdentifier procedure, Dictionary<string, object> args, int timeout, int? ifNoReturn, bool withStatusMessage);

        public abstract IPreparedExec PrepareExec(DbConnection connection, ILogger logger, string logPrefix = null, bool catchExceptions = false);
    }
}
