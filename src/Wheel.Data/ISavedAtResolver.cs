﻿using Microsoft.EntityFrameworkCore.ChangeTracking;
using System;
using System.Collections.Generic;
using System.Text;

namespace Wheel.Data
{
    /// <summary>
    /// Resolve InsertedAt / UpdatedAt value. Used by <see cref="SaveChangesTracker.SaveChanges(ChangeTracker)"/>
    /// </summary>
    public interface ISavedAtResolver
    {
        /// <summary>
        /// Return DateTime object either in UTC or local time, depending on database rules.
        /// </summary>
        /// <returns></returns>
        DateTime GetAt();
    }
}
