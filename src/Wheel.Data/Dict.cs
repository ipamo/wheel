﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Wheel.Data
{
    /// <summary>
    /// Key-value pairs that can be stored in the database.
    /// </summary>
    [AutoColumns]
    public class Dict
    {
        [Key, Required, MaxLength(250)]
        public string Id { get; set; }

        [Required, MaxLength(250)]
        public string Value { get; set; }
    }
}
