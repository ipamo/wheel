﻿using Microsoft.EntityFrameworkCore.ChangeTracking;
using System;
using System.Collections.Generic;
using System.Text;

namespace Wheel.Data
{
    /// <summary>
    /// Resolve InsertedById / UpdatedById value. Used by <see cref="SaveChangesTracker.SaveChanges(ChangeTracker)"/>
    /// </summary>
    public interface ISavedByResolver
    {
        object GetId();
    }
}
