// Adapted from .NET Foundation's CommandLineUtils.
// Copyright (c) .NET Foundation. All rights reserved.
// Licensed under the Apache License, Version 2.0. See License.txt in the project root for license information.

using System;
using System.Collections.Generic;
using System.Linq;

namespace Wheel.Commands
{
    /// <summary>
    /// Describe an option for the command. Should be defined using one of the <c>Option</c> methods of <see cref="ICommandBuilder"/>.
    /// </summary>
    public class CommandOption
    {
        private readonly List<string> _rawValues = new List<string>();

        public CommandOption(string template)
        {
            Template = template;

            foreach (var part in Template.Split(new[] { ' ', '|' }, StringSplitOptions.RemoveEmptyEntries))
            {
                if (part.StartsWith("--"))
                {
                    LongName = part.Substring(2);
                }
                else if (part.StartsWith("-"))
                {
                    var optName = part.Substring(1);

                    // If there is only one char and it is not an English letter, it is a symbol option (e.g. "-?")
                    if (optName.Length == 1 && !IsEnglishLetter(optName[0]))
                    {
                        SymbolName = optName;
                    }
                    else
                    {
                        ShortName = optName;
                    }
                }
                else if (part.StartsWith("<") && part.EndsWith(">"))
                {
                    ValueName = part.Substring(1, part.Length - 2);
                }
                else
                {
                    throw new ArgumentException($"Invalid template pattern '{template}'", nameof(template));
                }
            }

            if (string.IsNullOrEmpty(LongName) && string.IsNullOrEmpty(ShortName) && string.IsNullOrEmpty(SymbolName))
            {
                throw new ArgumentException($"Invalid template pattern '{template}'", nameof(template));
            }
        }
        
        /// <summary>
        /// Name(s) of the option, for example: <c>-o|--option</c>.
        /// </summary>
        public string Template { get; }

        /// <summary>
        /// Short description of the option displayed in help text. For example: <c>An option.</c>.
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// Indicate that the option does not expect any value. Use <see cref="CommandOption.IsEnabled"/> to know if the option was provided at least once,
        /// or <see cref="CommandOption.Count"/> to know how many times the option was provided.
        /// </summary>
        public bool NoValue { get; set; }

        /// <summary>
        /// Indicate whether the option can have multiple values.
        /// </summary>
        public bool Multiple { get; set; }

        /// <summary>
        /// Indicate whether the option is required. If it is required and not given, a <see cref="CommandParserException"/> will be raised
        /// when using <see cref="CommandOption.Value"/> property.
        /// </summary>
        public bool Required { get; set; }

        /// <summary>
        /// Indicate that the option is defined for the current command and all sub commands.
        /// </summary>
        public bool Inherited { get; set; }

        /// <summary>
        /// When disabled, the option does not appear in the list of options and argument of the command's help text. Enabled by default.
        /// </summary>
        public bool ShowInHelp { get; set; } = true;
        
        /// <summary>
        /// Get the short name of the option without the leading <c>-</c>, if it has been defined.
        /// </summary>
        public string ShortName { get; }

        /// <summary>
        /// Get the long name of the option without the leading <c>--</c>, if it has been defined.
        /// </summary>
        public string LongName { get; }

        /// <summary>
        /// Get the value name of the option without the leading and trailing <c>&lt;</c> and <c>&gt;</c>.
        /// </summary>
        public string ValueName { get; }

        /// <summary>
        /// Get the symbol name of the option (for short names with only one char that is not an English letter).
        /// </summary>
        public string SymbolName { get; }

        /// <summary>
        /// Get the long name, or the value name, or the short name, or the symbol name of the option, whichever is defined (in this order).
        /// </summary>
        public string Name => LongName ?? ValueName ?? ShortName ?? SymbolName;

        internal void Clear()
        {
            _rawValues.Clear();
        }
        
        internal bool TryParse(string value)
        {
            if (NoValue)
            {
                if (value != null)
                    return false;

                _rawValues.Add("true");
                return true;
            }
            else if (Multiple)
            {
                _rawValues.Add(value);
                return true;
            }
            else // SingleValue
            {
                if (_rawValues.Any())
                    return false;

                _rawValues.Add(value);
                return true;
            }
        }

        /// <summary>
        /// Get all values of the option (intended for multiple-value options).
        /// </summary>
        public List<string> Values
        {
            get
            {
                if (Required)
                {
                    var firstValue = _rawValues.FirstOrDefault();
                    if (string.IsNullOrEmpty(firstValue))
                        throw new CommandParserException($"Option '{Name}' is required");
                }
                return _rawValues;
            }
        }

        /// <summary>
        /// Indicate whether a value has been assigned for the option.
        /// </summary>
        public bool HasValue => _rawValues.Any();

        /// <summary>
        /// Get the value of the option (intended for non-multiple-value options).
        /// </summary>
        public string Value => Values.FirstOrDefault();

        /// <summary>
        /// Get the value of the option as an integer. Throws an exception if the provided value is not an integer.
        /// </summary>
        public int IntValue
        {
            get
            {
                var value = _rawValues.FirstOrDefault();
                if (string.IsNullOrEmpty(value))
                {
                    if (Required)
                        throw new CommandParserException($"Option '{Name}' is required");
                    else
                        return default(int);
                }
                if (! int.TryParse(value, out int intValue))
                    throw new CommandParserException($"Option '{Name}' must be an integer");
                return intValue;
            }
        }
        
        /// <summary>
        /// Indicate that the option was enabled (intended for no-value options).
        /// </summary>
        public bool IsEnabled => _rawValues.Any();

        /// <summary>
        /// Indicate the number of times the option was given (intended for no-value options).
        /// </summary>
        public int Count => Values.Count;


        #region Helpers

        private static bool IsEnglishLetter(char c)
        {
            return (c >= 'a' && c <= 'z') || (c >= 'A' && c <= 'Z');
        }

        #endregion
    }
}
