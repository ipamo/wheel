﻿using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Text;
using Wheel.Commands.Internal;

namespace Wheel.Commands
{
    /// <summary>
    /// Base class for defining command line applications. Intended to be used in the application's <c>Program.Main</c> method.
    /// </summary>
    public static class CommandApplication
    {
        /// <summary>
        /// Create a command application builder.
        /// </summary>
        public static ICommandApplicationBuilder CreateDefaultBuilder()
        {
            return new CommandApplicationBuilder();
        }
    }
}
