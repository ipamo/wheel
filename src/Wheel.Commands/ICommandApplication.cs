﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Wheel.Commands
{
    /// <summary>
    /// Command-line application.
    /// </summary>
    public interface ICommandApplication
    {
        /// <summary>
        /// Application service provider.
        /// </summary>
        IServiceProvider Services { get; }

        /// <summary>
        /// Run the application.
        /// </summary>
        int Run(params string[] args);
    }
}
