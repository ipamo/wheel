﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;

namespace Wheel.Commands.Internal
{
    internal class CommandRunnerStatus
    {
        public string CommandName { get; set; }
        public Thread Thread { get; set; }
        public int? ReturnCode { get; set; }
    }
}
