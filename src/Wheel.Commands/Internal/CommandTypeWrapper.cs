﻿using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Reflection;
using System.Text;
#if !NET20
using System.ComponentModel.DataAnnotations;
#endif

namespace Wheel.Commands.Internal
{
    /// <summary>
    /// A wrapper for running commands defined with <see cref="CommandAttribute"/>.
    /// </summary>
    internal class CommandTypeWrapper
    {
        private readonly Type _commandType;
        private readonly ICommandBuilder _cmd;
        private readonly WheelInfra _infra;

        public CommandTypeWrapper(Type commandType, ICommandBuilder cmd, WheelInfra infra)
        {
            _commandType = commandType;
            _cmd = cmd;
            _infra = infra;
        }

        private MethodInfo _runMethod;

        private Dictionary<CommandOption, PropertyInfo> _options;
        private List<CommandArgument> _argumentsList;
        private Dictionary<CommandArgument, ParameterInfo> _arguments;

        public void ConfigureOptionsArguments()
        {
            // List option properties
            var optionProperties = _commandType.GetProperties()
                .Where(p => p.GetCustomAttributes(inherit: false).Any(a => a.GetType() == typeof(CommandOptionAttribute))
                        && p.GetGetMethod() != null
                        && !p.GetGetMethod().IsStatic);

            // Configure options
            _options = new Dictionary<CommandOption, PropertyInfo>();
            foreach (var prop in optionProperties)
            {
                var attr = (CommandOptionAttribute)prop.GetCustomAttributes(inherit: false).First(a => a.GetType() == typeof(CommandOptionAttribute));

                var required = attr.Required;
#if !NET20
                if (!required && prop.GetCustomAttributes(inherit: false).Any(a => a.GetType() == typeof(RequiredAttribute)))
                    required = true;
#endif

                if (prop.GetGetMethod() == null)
                    throw new CommandDefinitionException(string.Format("Property {0} of type {1} cannot be used as a command option: no public get method", prop.Name, _commandType));
                if (prop.GetSetMethod() == null)
                    throw new CommandDefinitionException(string.Format("Property {0} of type {1} cannot be used as a command option: no public set method", prop.Name, _commandType));

                var option = _cmd.Option(template: attr.Template ?? GetOptionTemplate(prop),
                    description: attr.Description ?? GetOptionDescription(prop),
                    configuration: o =>
                    {
                        o.NoValue = prop.PropertyType == typeof(bool) || prop.PropertyType == typeof(bool?);
                        o.Multiple = prop.PropertyType.IsArray;
                        o.Required = required;
                        o.ShowInHelp = attr.ShowInHelp;
                    });
                _options[option] = prop;
            }

            // Configure arguments
            _runMethod = GetRunMethod();
             _arguments = new Dictionary<CommandArgument, ParameterInfo>();
            _argumentsList = new List<CommandArgument>();

            ParameterInfo prevWasMultiple = null;

            foreach (var param in _runMethod.GetParameters())
            {
                if (prevWasMultiple != null)
                    throw new CommandDefinitionException(string.Format("Command type {0}: multi-value parameter ({1}) of the Run method must be the last parameter", _commandType, prevWasMultiple.Name));

                var multiple = param.ParameterType.IsArray;
                if (multiple)
                    prevWasMultiple = param;

                var required = false;
#if !NET20
                if (!required && param.GetCustomAttributes(inherit: false).Any(a => a.GetType() == typeof(RequiredAttribute)))
                    required = true;
#endif

                var argument = _cmd.Argument(name: param.Name,
                    description: null,
                    configuration: o =>
                    {
                        o.Multiple = multiple;
                        o.Required = required;
                    });

                _argumentsList.Add(argument);
                _arguments[argument] = param;
            }
        }
        
        public int Run()
        {
            var instance = CreateInstance();

            // Set option properties
            foreach (var e in _options)
            {
                var option = e.Key;

                if (option.HasValue)
                {
                    var prop = e.Value;
                    if (option.Multiple)
                        prop.SetValue(instance, ConvertTo(option.Values, prop.PropertyType), null);
                    else
                        prop.SetValue(instance, ConvertTo(option.Value, prop.PropertyType), null);
                }
                // else, the default option value applies
            }

            // Set argument parameters
            var parameters = new object[_argumentsList.Count];
            int i = 0;
            foreach (var argument in _argumentsList)
            {
                var type = _arguments[argument].ParameterType;
                if (argument.HasValue || _arguments[argument].DefaultValue == DBNull.Value)
                {
                    if (argument.Multiple)
                        parameters[i] = ConvertTo(argument.Values, type);
                    else
                        parameters[i] = ConvertTo(argument.Value, type);
                }
                else if (_arguments[argument].DefaultValue != DBNull.Value)
                {
                    parameters[i] = _arguments[argument].DefaultValue;
                }
                
                i++;
            }

            object returnValue;
            try
            {
                returnValue = _runMethod.Invoke(instance, parameters);
            }
            catch (TargetInvocationException ex) when (ex.InnerException != null)
            {
                var e = ex.InnerException;
                // Will be caught by CommandRunner and handled nicely
                throw new MessageException(e.Message, e);
            }

            if (_runMethod.ReturnType == typeof(int))
                return (int)returnValue;
            else
                return ReturnCode.OK;
        }

        private object CreateInstance()
        {
            if (_infra.IsDependencyInjectionEnabled)
                return _infra.ApplicationServices.GetRequiredService(_commandType);
            else
                return ReflectionUtils.CreateInstance(_commandType, _infra.GetInfraServicesDictionary());
        }

        private MethodInfo GetRunMethod()
        {
            var methods = _commandType.GetMethods().Where(m => m.IsPublic && m.Name == "Run").ToArray();
            if (methods.Length == 0)
                throw new CommandDefinitionException(string.Format("Cannot use CommandAttribute on type {0}: no public Run method", _commandType));
            if (methods.Length > 2)
                throw new CommandDefinitionException(string.Format("Cannot use CommandAttribute on type {0}: several public Run methods", _commandType));
            var method = methods[0];
            if (method.IsStatic)
                throw new CommandDefinitionException(string.Format("Cannot use CommandAttribute on type {0}: public Run method must not be static", _commandType));
            if (! (method.ReturnType == typeof(int) || method.ReturnType == typeof(void)))
                throw new CommandDefinitionException(string.Format("Cannot use CommandAttribute on type {0}: public Run method must have return type int or void", _commandType));
            return method;
        }

        internal static string GetOptionTemplate(PropertyInfo prop)
        {
            return "--" + prop.Name.ToLowerSnake(separator: '-');
        }

        internal static string GetOptionDescription(PropertyInfo prop)
        {
            var type = prop.PropertyType;
            var underlyingType = Nullable.GetUnderlyingType(type); // null if type is not nullable

            if (type.IsEnum)
            {
                return string.Join(" | ", Enum.GetValues(type).Cast<object>().Select(v => v.ToString()).ToArray());
            }
            else if (underlyingType != null && underlyingType.IsEnum)
            {
                return string.Join(" | ", Enum.GetValues(underlyingType).Cast<object>().Select(v => v.ToString()).ToArray());
            }

            return null;
        }

        /// <summary>
        /// Convert a string to a type.
        /// </summary>
        /// <remarks>Internal for testing.</remarks>
        /// <param name="str">The string to convert.</param>
        /// <param name="type">The target type.</param>
        /// <returns>Converted string.</returns>
        /// <exception cref="InvalidCastException">Conversion error.</exception>
        internal static object ConvertTo(string str, Type type)
        {
            if (type.IsArray)
                throw new InvalidOperationException($"String cannot be converted to {type} because it is an array");
            if (str == null)
                return type.IsValueType ? Activator.CreateInstance(type) : null;

            var underlyingType = Nullable.GetUnderlyingType(type); // null if type is not nullable
            
            try
            {
                if (type == typeof(bool) || type == typeof(bool?))
                {
                    return str == "1" || str == "true";
                }
                else if (type == typeof(DateTime) || type == typeof(DateTime?))
                {
                    if (str.Length == 10)
                        return DateTime.ParseExact(str, "yyyy-MM-dd", CultureInfo.InvariantCulture);
                    else
                        return DateTime.ParseExact(str, "yyyy-MM-dd HH:mm:ss", CultureInfo.InvariantCulture);
                }
                else if (type.IsEnum)
                {
                    return Enum.Parse(type, str, true);
                }
                else if (underlyingType != null && underlyingType.IsEnum)
                {
                    return Enum.Parse(underlyingType, str, true);
                }
                else
                {
                    return Convert.ChangeType(str, type);
                }
            }
            catch (Exception e)
            {
                throw new InvalidOperationException($"Cannot convert \"{str}\" to {type}: {e.Message}");
            }
        }

        private static object ConvertTo(IEnumerable<string> strs, Type type)
        {
            if (!type.IsArray)
                throw new InvalidOperationException($"String array cannot be converted to {type} because it is not an array");
            var elementType = type.GetElementType();

            if (strs == null)
                return Array.CreateInstance(elementType, 0);

            var length = strs.Count();
            Array array = Array.CreateInstance(elementType, length);
            int i = 0;
            foreach (var str in strs)
            {
                var value = ConvertTo(str, elementType);
                array.SetValue(value, i);
                i++;
            }
            return array;
        }
    }
}
