// Adapted from .NET Foundation's CommandLineUtils.
// Copyright (c) .NET Foundation. All rights reserved.
// Licensed under the Apache License, Version 2.0. See License.txt in the project root for license information.

using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Reflection;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using System.Diagnostics;
using System.Threading;

namespace Wheel.Commands.Internal
{
    internal static class CommandParser
    {
        private static readonly bool _allowArgumentSeparator = true;
        private static readonly bool _throwOnUnexpectedArg = true;

        private static void ClearCommand(CommandBuilder command)
        {
            command.ParsedArguments = null;
            command.ParsedReport = false;
            command.ParsedReportTo = null;

            foreach (var opt in GetOptions(command))
                opt.Clear();
            foreach (var arg in command.Arguments)
                arg.Clear();
        }
        
        public static CommandBuilder Parse(CommandBuilder command, params string[] args)
        {
            ClearCommand(command);

            CommandOption option = null;
            IEnumerator<CommandArgument> arguments = null;
            List<string> argsWithoutCommand = new List<string>(args.Length);
            
            for (var index = 0; index < args.Length; index++)
            {
                var arg = args[index];
                argsWithoutCommand.Add(arg);

                var processed = false;
                if (!processed && option == null)
                {
                    string[] longOption = null;
                    string[] shortOption = null;

                    if (arg.StartsWith("--"))
                    {
                        longOption = arg.Substring(2).Split(new[] { ':', '=' }, 2);
                    }
                    else if (arg.StartsWith("-"))
                    {
                        shortOption = arg.Substring(1).Split(new[] { ':', '=' }, 2);
                    }

                    if (longOption != null)
                    {
                        processed = true;
                        var longOptionName = longOption[0];
                        option = GetOptions(command).SingleOrDefault(opt => string.Equals(opt.LongName, longOptionName, StringComparison.Ordinal));

                        if (option == null)
                        {
                            if (string.IsNullOrEmpty(longOptionName) && !_throwOnUnexpectedArg && _allowArgumentSeparator)
                            {
                                // skip over the '--' argument separator
                                index++;
                            }

                            HandleUnexpectedArg(command, args, index, argTypeName: "option");
                            break;
                        }

                        // If we find a help/version option, show information and stop parsing
                        if (command.OptionHelp == option)
                        {
                            ShowHelp(command);
                            return null;
                        }
                        else if (command.OptionVersion == option)
                        {
                            ShowVersion(command);
                            return null;
                        }
                        else if (command.OptionReport == option)
                        {
                            command.ParsedReport = true;
                            argsWithoutCommand.RemoveAt(argsWithoutCommand.Count - 1);
                        }

                        if (longOption.Length == 2)
                        {
                            if (!option.TryParse(longOption[1]))
                            {
                                ShowHint(command);
                                throw new CommandParserException($"Unexpected value '{longOption[1]}' for option '{option.LongName}'");
                            }
                            option = null;
                        }
                        else if (option.NoValue)
                        {
                            // No value is needed for this option
                            option.TryParse(null);
                            option = null;
                        }
                    }

                    if (shortOption != null)
                    {
                        processed = true;
                        option = GetOptions(command).SingleOrDefault(opt => string.Equals(opt.ShortName, shortOption[0], StringComparison.Ordinal));

                        // If not a short option, try symbol option
                        if (option == null)
                        {
                            option = GetOptions(command).SingleOrDefault(opt => string.Equals(opt.SymbolName, shortOption[0], StringComparison.Ordinal));
                        }

                        if (option == null)
                        {
                            HandleUnexpectedArg(command, args, index, argTypeName: "option");
                            break;
                        }

                        // If we find a help/version option, show information and stop parsing
                        if (command.OptionHelp == option)
                        {
                            ShowHelp(command);
                            return null;
                        }
                        else if (command.OptionVersion == option)
                        {
                            ShowVersion(command);
                            return null;
                        }

                        if (shortOption.Length == 2)
                        {
                            if (!option.TryParse(shortOption[1]))
                            {
                                ShowHint(command);
                                throw new CommandParserException($"Unexpected value '{shortOption[1]}' for option '{option.LongName}'");
                            }
                            option = null;
                        }
                        else if (option.NoValue)
                        {
                            // No value is needed for this option
                            option.TryParse(null);
                            option = null;
                        }
                    }
                }

                if (!processed && option != null)
                {
                    processed = true;
                    if (!option.TryParse(arg))
                    {
                        ShowHint(command);
                        throw new CommandParserException($"Unexpected value '{arg}' for option '{option.LongName}'");
                    }
                    if (command.OptionReportTo == option)
                    {
                        var count = argsWithoutCommand.Count;
                        argsWithoutCommand.RemoveAt(count - 1);
                        argsWithoutCommand.RemoveAt(count - 2);
                        command.ParsedReportTo = option.Value;
                    }
                    option = null;
                }

                if (!processed && arguments == null)
                {
                    var currentCommand = command;
                    foreach (var subcommand in command.Subcommands)
                    {
                        if (string.Equals(subcommand.Name, arg, StringComparison.OrdinalIgnoreCase))
                        {
                            processed = true;
                            command = subcommand;
                            ClearCommand(command);
                            argsWithoutCommand.RemoveAt(argsWithoutCommand.Count - 1);
                            break;
                        }
                    }

                    // If we detect a subcommand
                    if (command != currentCommand)
                    {
                        processed = true;
                    }
                }
                if (!processed)
                {
                    if (arguments == null)
                    {
                        arguments = new CommandArgumentEnumerator(command.Arguments.GetEnumerator());
                    }
                    if (arguments.MoveNext())
                    {
                        processed = true;
                        arguments.Current.AddValue(arg);
                    }
                }
                if (!processed)
                {
                    HandleUnexpectedArg(command, args, index, argTypeName: "command or argument");
                    break;
                }
            }

            if (option != null)
            {
                ShowHint(command);
                throw new CommandParserException($"Missing value for option '{option.LongName}'");
            }
            
            if (command.Func == null)
            {
                if (command.DefaultSubcommand != null)
                {
                    command = command.DefaultSubcommand;
                    return Parse(command, args);
                }
            }
            
            command.ParsedArguments = argsWithoutCommand.ToArray();
            return command;
        }

        private static void ShowHint(CommandBuilder command)
        {
            if (command.OptionHelp != null)
            {
                Console.Error.WriteLine(string.Format("Specify --{0} for a list of available options and commands.", command.OptionHelp.LongName));
            }
        }

        private static void ShowHelp(CommandBuilder command)
        {
            Console.Error.WriteLine(GetHelpText(command));
        }

        private static void ShowVersion(CommandBuilder command)
        {
            Console.Error.WriteLine("{0} {1} ({2})", command.Name, command.VersionGetter(), command.Infra.Environment.EnvironmentName);
        }

        private static void HandleUnexpectedArg(CommandBuilder command, string[] args, int index, string argTypeName)
        {
            if (_throwOnUnexpectedArg)
            {
                ShowHint(command);
                throw new CommandParserException($"Unrecognized {argTypeName} '{args[index]}'");
            }
            else
            {
                // All remaining arguments are stored for further use
                var remainingArgs = new string[args.Length];
                for (var i = 0; i < args.Length; i++)
                    remainingArgs[i] = args[index + i];
                command.RemainingArguments.AddRange(remainingArgs);
            }
        }

        private static string GetHelpText(CommandBuilder command)
        {
            var headerBuilder = new StringBuilder("Usage:");
            for (ICommandBuilder cmd = command; cmd != null; cmd = cmd.Parent)
            {
                if (cmd.Name != CommandBuilder.DEFAULT_COMMAND_NAME)
                    headerBuilder.Insert(6, string.Format(" {0}", cmd.Name));
            }

            var optionsBuilder = new StringBuilder();
            var commandsBuilder = new StringBuilder();
            var argumentsBuilder = new StringBuilder();

            var arguments = command.Arguments.Where(a => a.ShowInHelp).ToList();
            if (arguments.Any())
            {
                headerBuilder.Append(" [arguments]");

                argumentsBuilder.AppendLine();
                argumentsBuilder.AppendLine("Arguments:");
                var maxArgLen = arguments.Max(a => a.Name.Length);
                var outputFormat = string.Format("  {{0, -{0}}}{{1}}", maxArgLen + 2);
                foreach (var arg in arguments)
                {
                    argumentsBuilder.AppendFormat(outputFormat, arg.Name, arg.Description);
                    argumentsBuilder.AppendLine();
                }
            }

            var options = GetOptions(command).Where(o => o.ShowInHelp).ToList();
            if (options.Any())
            {
                headerBuilder.Append(" [options]");

                optionsBuilder.AppendLine();
                optionsBuilder.AppendLine("Options:");
                var maxOptLen = options.Max(o => o.Template.Length);
                var outputFormat = string.Format("  {{0, -{0}}}{{1}}", maxOptLen + 2);
                foreach (var opt in options)
                {
                    optionsBuilder.AppendFormat(outputFormat, opt.Template, opt.Description);
                    optionsBuilder.AppendLine();
                }
            }

            var commands = command.Subcommands.Where(c => c.ShowInHelp).ToList();
            if (commands.Any())
            {
                headerBuilder.Append(" [command]");

                commandsBuilder.AppendLine();
                commandsBuilder.AppendLine("Commands:");
                var maxCmdLen = commands.Max(c => c.Name.Length);
                var outputFormat = string.Format("  {{0, -{0}}}{{1}}", maxCmdLen + 2);
                foreach (var cmd in commands.OrderBy(c => c.Name))
                {
                    string name;
                    if (cmd == command.DefaultSubcommand && cmd.Name != CommandBuilder.DEFAULT_COMMAND_NAME)
                        name = CommandBuilder.DEFAULT_COMMAND_NAME + cmd.Name;
                    else
                        name = cmd.Name;
                    commandsBuilder.AppendFormat(outputFormat, name, cmd.Description);
                    commandsBuilder.AppendLine();
                }

                if (command.OptionHelp != null)
                {
                    commandsBuilder.AppendLine();
                    commandsBuilder.AppendFormat($"Use \"{command.Name} [command] --{command.OptionHelp.LongName}\" for more information about a command.");
                    commandsBuilder.AppendLine();
                }
            }

            if (_allowArgumentSeparator)
            {
                headerBuilder.Append(" [[--] <arg>...]");
            }

            headerBuilder.AppendLine();

            var nameAndVersion = new StringBuilder();
            if (command.Parent == null)
            {
                var version = command.VersionGetter != null ? command.VersionGetter() : null;
                if (!(string.IsNullOrEmpty(command.Name) && string.IsNullOrEmpty(version)))
                {
                    nameAndVersion.AppendLine(command.Name + " " + version + " (" + command.Infra.Environment.EnvironmentName + ")");
                    nameAndVersion.AppendLine();
                }
            }

            var textualInformation = new StringBuilder();
            if (!string.IsNullOrEmpty(command.Description) && !string.IsNullOrEmpty(command.HelpText))
            {
                textualInformation.AppendLine();
                textualInformation.AppendLine(command.Description);
                textualInformation.AppendLine();
                textualInformation.AppendLine(command.HelpText);
            }
            else if (!string.IsNullOrEmpty(command.Description))
            {
                textualInformation.AppendLine();
                textualInformation.AppendLine(command.Description);
            }
            else if (!string.IsNullOrEmpty(command.HelpText))
            {
                textualInformation.AppendLine();
                textualInformation.AppendLine(command.HelpText);
            }

            return nameAndVersion.ToString()
                + headerBuilder.ToString()
                + argumentsBuilder.ToString()
                + optionsBuilder.ToString()
                + commandsBuilder.ToString()
                + textualInformation.ToString();
        }

        private static IEnumerable<CommandOption> GetOptions(CommandBuilder command)
        {
            var expr = command.Options.AsEnumerable();
            var rootNode = command;
            while (rootNode.Parent != null)
            {
                rootNode = (CommandBuilder)rootNode.Parent;
                expr = expr.Concat(rootNode.Options.Where(o => o.Inherited));
            }

            return expr;
        }
        
        private class CommandArgumentEnumerator : IEnumerator<CommandArgument>
        {
            private readonly IEnumerator<CommandArgument> _enumerator;

            public CommandArgumentEnumerator(IEnumerator<CommandArgument> enumerator)
            {
                _enumerator = enumerator;
            }

            public CommandArgument Current
            {
                get
                {
                    return _enumerator.Current;
                }
            }

            object IEnumerator.Current
            {
                get
                {
                    return Current;
                }
            }

            public void Dispose()
            {
                _enumerator.Dispose();
            }

            public bool MoveNext()
            {
                if (Current == null || !Current.Multiple)
                {
                    return _enumerator.MoveNext();
                }

                // If current argument allows multiple values, we don't move forward and
                // all later values will be added to current CommandArgument.Values
                return true;
            }

            public void Reset()
            {
                _enumerator.Reset();
            }
        }
    }
}
