﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Text;
using Wheel.Commands.Internal;

namespace Wheel.Commands
{
    /// <summary>
    /// Helper methods for command applications.
    /// </summary>
    public static class CommandUtils
    {
        public static ICommandApplicationBuilder UseStartup<TStartup>(this ICommandApplicationBuilder builder)
            where TStartup : class
            => UseStartup(builder, typeof(TStartup));

        public static ICommandApplicationBuilder UseStartup(this ICommandApplicationBuilder builder, Type tstartup)
        {
            builder.ConfigureRootCommand(root => ((CommandBuilder)root).Define(tstartup));
            return builder;
        }

        internal static void DebugBuilder(CommandBuilder cmd, TextWriter wr = null, string space = "")
        {
            if (wr == null)
                wr = Console.Out;

            wr.WriteLine(space + $"Command {cmd.Name}:");

            foreach (var o in cmd.Options)
            {
                string config = null;
                if (o.NoValue)
                    config = (config == null ? " (" : config + ", ") + "no value";
                if (o.Multiple)
                    config = (config == null ? " (" : config + ", ") + "multiple";
                if (o.Required)
                    config = (config == null ? " (" : config + ", ") + "required";
                if (config != null)
                    config += ")";
                wr.WriteLine(space + $"- Option {o.Name}{config}: {(o.HasValue ? string.Join(", ", o.Values.ToArray()) : "<null>")}");
            }

            int i = 1;
            foreach (var o in cmd.Arguments)
            {
                string config = null;
                if (o.Multiple)
                    config = (config == null ? ", " : config + ", ") + "multiple";
                if (o.Required)
                    config = (config == null ? ", " : config + ", ") + "required";
                wr.WriteLine(space + $"- Argument #{i} ({o.Name ?? "not named"}{config}): {(o.HasValue ? string.Join(", ", o.Values.ToArray()) : "<null>")}");
                i++;
            }
            
            foreach (var o in cmd.Subcommands)
            {
                DebugBuilder(o, wr, "    " + space);
            }
        }
    }
}
