﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Wheel.Commands
{
    /// <summary>
    /// Settings section <c>[CommandApplication]</c>, used to configure the command application.
    /// </summary>
    public class CommandApplicationOptions
    {
        /// <summary>
        /// If true, an email report will be sent even if the --report option is not passed on the command line.
        /// If false, an email report will never be sent.
        /// If null, an email report will be sent if the --report option is passed on the command line.
        /// </summary>
        public bool? ReportEmailEnabled { get; set; }

        /// <summary>
        /// Destinataire(s) par défaut de l'email récapitulatif.
        /// </summary>
        public string ReportEmailTo { get; set; }

        /// <summary>
        /// Durée en millisecondes à attendre après l'envoi de l'email récapitulatif avant d'arrêter le programme (nécessaire avec certains serveurs SMTP, notamment celui de la RTM (500 ms), pour que l'email soit effectivement envoyé).
        /// </summary>
        public int SleepAfterReportSent { get; set; }

        /// <summary>
        /// Force the reporters to be of type <see cref="HtmlReporter"/>, even if email reporting is disabled.
        /// </summary>
        public bool ForceHtmlReporter { get; set; }
    }
}

