﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using Microsoft.Data.SqlClient;
using System.Text;

namespace Wheel.Data
{
    /// <summary>
    /// Default DBMS features for Sql Server.
    /// </summary>
    public class SqlServerDbms : BaseDbms
    {
        public SqlServerDbms(IConfiguration config,
                 ILoggerFactory loggerFactory,
                string defaultCskey = null)
            : base(config,
                  loggerFactory,
                  defaultCskey)
        {
            
        }


        public override Type DbConnectionType => typeof(SqlConnection);

        public override string DefaultSchema => "dbo";
        public override string DateOnlyNowSql => "(getdate())"; // must be written exactly like that to allow false incosistensies in CheckDatabaseConsistency.
        public override string DateTimeNowSql => "(getdate())"; // must be written exactly like that to allow false incosistensies in CheckDatabaseConsistency.
        

        protected override char IdentifierProtectBegin => '[';
        protected override char IdentifierProtectEnd => ']';
        public override DbConnection InstanciateConnection(string connectionString) => new SqlConnection(connectionString);

        public override string GetDatabaseName(DbConnection connection)
        {
            using (var cmd = new SqlCommand("SELECT DB_NAME()", (SqlConnection)connection))
            {
                return (string)cmd.ExecuteScalar();
            }
        }


        #region Component
        
        private string _componentGetSql;

        private void PrepareComponentSql()
        {
            if (_componentGetSql == null)
            {
                var table = GetTableName(typeof(Component));
                var idColumn = GetColumnName(typeof(Component), nameof(Component.Id));
                var nameColumn = GetColumnName(typeof(Component), nameof(Component.Name));

                _componentGetSql = $@"DECLARE @Id bigint
SET @Id = (SELECT {idColumn} FROM {table} WHERE {nameColumn} = @Name)
IF @Id IS NULL
BEGIN
    DECLARE @TableInsertedId TABLE (Id smallint NOT NULL)

	INSERT INTO {table} ({nameColumn})
    OUTPUT inserted.{idColumn} INTO @TableInsertedId
    VALUES (@Name)

    SET @Id = (SELECT Id FROM @TableInsertedId)
END

SELECT @Id";
            }
        }

        private readonly Dictionary<string, short> _componentCache = new Dictionary<string, short>();

        /// <summary>
        /// Renvoit l'identifiant du composant (table TT_Composant) portant le libellé donné.
        /// Le composant est créé s'il n'existe pas déjà.
        /// </summary>
        public override short GetComponentId(string name)
        {
            lock (_componentCache)
            {
                if (_componentCache.ContainsKey(name))
                    return _componentCache[name];

                PrepareComponentSql();

                using (var cmd = new SqlCommand(_componentGetSql, (SqlConnection)DefaultConnection))
                {
                    cmd.Parameters.AddWithValue("@Name", name);
                    var id = Convert.ToInt16(cmd.ExecuteScalar());
                    _componentCache[name] = id;
                    return id;
                }
            }
        }

        #endregion


        #region Dict
        
        private string _dictSetSql;
        private string _dictGetSql;
        private string _dictRemoveSql;

        private void PrepareDictSql()
        {
            if (_dictSetSql == null)
            {
                var componentId = GetComponentId<SqlServerDbms>();
                var table = GetTableName(typeof(Dict));
                var idColumn = GetColumnName(typeof(Dict), nameof(Dict.Id));
                var valueColumn = GetColumnName(typeof(Dict), nameof(Dict.Value));
                var insertedByComponentIdColumn = GetColumnName(typeof(Dict), InsertedThroughIdProperty);
                var updatedByComponentIdColumn = GetColumnName(typeof(Dict), UpdatedThroughIdProperty);
                var updatedAtColumn = GetColumnName(typeof(Dict), UpdatedAtProperty);

                _dictSetSql = $@"MERGE INTO {table} d
USING (VALUES (@Id, @Value)) AS s ({idColumn}, {valueColumn})
ON d.{idColumn} = s.{idColumn}
WHEN MATCHED THEN
UPDATE SET
    {valueColumn} = s.{valueColumn},
    {updatedByComponentIdColumn} = {componentId},
    {updatedAtColumn} = {DateTimeNowSql}
WHEN NOT MATCHED THEN
    INSERT ({idColumn}, {valueColumn}, {insertedByComponentIdColumn})
    VALUES (s.{idColumn}, s.{valueColumn}, {componentId});";

                _dictGetSql = $@"SELECT {valueColumn} FROM {table} WHERE {idColumn} = @Id";

                _dictRemoveSql = $@"DELETE FROM {table} WHERE {idColumn} = @Id";
            }
        }
        
        protected override void SetDictValue(string key, string value)
        {
            PrepareDictSql();

            using (var cmd = new SqlCommand(_dictSetSql, (SqlConnection)DefaultConnection))
            {
                cmd.Parameters.AddWithValue("@Id", key);
                cmd.Parameters.AddWithValue("@Value", value);
                cmd.ExecuteNonQuery();
            }
        }

        public override string GetDictValue(string key)
        {
            PrepareDictSql();

            using (var cmd = new SqlCommand(_dictGetSql, (SqlConnection)DefaultConnection))
            {
                cmd.Parameters.AddWithValue("@Id", key);
                var r = cmd.ExecuteScalar();
                if (r == null || r == DBNull.Value || (string)r == "")
                    return null;
                else
                    return (string)r;
            }
        }

        public override void RemoveDictValue(string key)
        {
            PrepareDictSql();

            using (var cmd = new SqlCommand(_dictRemoveSql, (SqlConnection)DefaultConnection))
            {
                cmd.Parameters.AddWithValue("@Id", key);
                cmd.ExecuteNonQuery();
            }
        }

        #endregion


        #region Stored procedures

        protected override Dictionary<string, object> GetNamedArgs(IPreparedExec prepared, DbIdentifier procedure, object[] args)
        {
            if (prepared == null)
                throw new ArgumentNullException(nameof(prepared));
            if (!(prepared is SqlServerPreparedExec))
                throw new ArgumentException("Argument 'prepared' must be of type SqlServerPreparedExec", nameof(prepared));
            var preparedCast = (SqlServerPreparedExec)prepared;

            Dictionary<string, object> argDictionary = null;
            if (args != null && args.Length >= 1)
            {
                var sanitized = procedure.Sanitize();

                var sql = $"SELECT name FROM sys.parameters s WHERE object_id = object_id('{sanitized}', 'P') AND (name != '@StatusMessage' OR is_output = 0) ORDER BY parameter_id";

                // Détermine le nom des paramètres
                var argNames = new List<string>();
                using (var cmd = new SqlCommand(sql, preparedCast.Connection))
                using (var r = cmd.ExecuteReader())
                {
                    while (r.Read())
                        argNames.Add(Convert.ToString(r[0]));
                }

                if (argNames.Count == 0)
                {
                    // On vérifie l'existence de la procédure stockée
                    sql = $"SELECT object_id('{sanitized}', 'P')";
                    using (var cmd = new SqlCommand(sql, preparedCast.Connection))
                    {
                        var r = cmd.ExecuteScalar();
                        if (r == DBNull.Value)
                            throw new InvalidOperationException($"Stored procedure '{sanitized}' not found.");
                    }
                }

                argDictionary = new Dictionary<string, object>();
                int i = 0;
                while (i < argNames.Count && i < args.Length)
                {
                    argDictionary.Add(argNames[i], args[i]);
                    i++;
                }


                if (args.Length > argNames.Count)
                {
                    var s1 = args.Length - argNames.Count > 1 ? "s" : "";
                    _logger.LogWarning($"{sanitized}: {(args.Length - argNames.Count)} argument{s1} ignored ({args.Length} given, {argNames.Count} expected)");
                }
            }

            return argDictionary;
        }

        protected override (int returnCode, string statusMessage) Exec(IPreparedExec prepared, DbIdentifier procedure, Dictionary<string, object> args, int timeout, int? ifNoReturn, bool withStatusMessage)
        {
            if (prepared == null)
                throw new ArgumentNullException(nameof(prepared));
            if (!(prepared is SqlServerPreparedExec))
                throw new ArgumentException("Argument 'prepared' must be of type SqlServerPreparedExec", nameof(prepared));
            var preparedCast = (SqlServerPreparedExec)prepared;
            
            var sanitized = procedure.Sanitize();

            if (timeout < 0)
                timeout = DefaultExecTimeout;

            using (SqlCommand cmd = preparedCast.Connection.CreateCommand())
            {
                cmd.CommandText = sanitized;
                cmd.CommandTimeout = timeout;
                cmd.CommandType = CommandType.StoredProcedure;

                if (args != null)
                {
                    foreach (var entry in args)
                    {
                        SqlParameter p = new SqlParameter(entry.Key, entry.Value);
                        cmd.Parameters.Add(p);
                    }
                }

                SqlParameter retval = cmd.Parameters.Add("@return", SqlDbType.Int);
                retval.Direction = ParameterDirection.ReturnValue;

                SqlParameter statusMessageParam;
                if (withStatusMessage)
                {
                    statusMessageParam = cmd.Parameters.Add("@StatusMessage", SqlDbType.NVarChar, -1);
                    statusMessageParam.Direction = ParameterDirection.Output;
                }
                else
                {
                    statusMessageParam = null;
                }

                _logger.LogInformation($"Exec procedure {sanitized} ..."); // We want to keep 'logger' only for the procedure logs
                try
                {
                    cmd.ExecuteNonQuery();
                }
                catch (SqlException e) when (preparedCast.CatchExceptions)
                {
                    preparedCast.Logger.LogSqlServer(e, prefix: preparedCast.LogPrefix);
                }

                string statusMessage;
                if (withStatusMessage && cmd.Parameters["@StatusMessage"] != null)
                {
                    statusMessage = Convert.ToString(cmd.Parameters["@StatusMessage"].Value);
                }
                else
                {
                    statusMessage = null;
                }
                
                if (cmd.Parameters["@return"] == null || cmd.Parameters["@return"].Value == null)
                {
                    // Cela arrive notamment lorsque l'instruction T-SQL THROW est utilisée
                    string message;
                    if (_lastSqlErrorCodes.TryGetValue(prepared, out int code) && code == 2812)
                    {
                        ifNoReturn = null; // we always want to raise the exception
                        message = $"Procedure {sanitized} does not exist.";
                    }
                    else
                    {
                        message = $"Procedure {sanitized} returned no value.";
                    }

                    if (ifNoReturn.HasValue)
                    {
                        _logger.LogDebug(message);
                        return (ifNoReturn.Value, statusMessage);
                    }
                    else if (preparedCast.CatchExceptions)
                    {
                        _logger.LogError(message);
                        return (-1, statusMessage);
                    }
                    else
                    {
                        throw new InvalidOperationException(message);
                    }
                }

                return (Convert.ToInt32(cmd.Parameters["@return"].Value), statusMessage);
            }
        }
        
        private Dictionary<IPreparedExec, int> _lastSqlErrorCodes = new Dictionary<IPreparedExec, int>();

        public override IPreparedExec PrepareExec(DbConnection connection, ILogger logger, string logPrefix = null, bool catchExceptions = false)
        {
            if (connection == null)
                throw new ArgumentNullException(nameof(connection));
            if (!(connection is SqlConnection))
                throw new ArgumentException($"SqlConnection required, got {connection.GetType()}", nameof(connection));

            if (logger == null)
                throw new ArgumentNullException(nameof(logger));

            if (connection.State == ConnectionState.Closed)
                connection.Open();

            var prepared = new SqlServerPreparedExec((SqlConnection)connection, logger, logPrefix, catchExceptions);

            ((SqlConnection)connection).FireInfoMessageEventOnUserErrors = true;

            ((SqlConnection)connection).InfoMessage += delegate (object sender, SqlInfoMessageEventArgs e)
            {
                foreach (var error in e.Errors)
                {
                    switch (error)
                    {
                        case SqlError se:
                            _lastSqlErrorCodes[prepared] = se.Number;
                            logger.LogSqlServer(se, prefix: logPrefix);
                            break;
                        default:
                            logger.LogError(error.ToString());
                            break;
                    }
                }
            };
            
            return prepared;
        }

        #endregion
    }
}
