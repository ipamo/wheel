﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Wheel.Ftp
{
    /// <summary>
    /// Exception raised within SafeTranfer() methods.
    /// </summary>
    public class SafeTransferException : Exception
    {
        public SafeTransferException(string message)
            : base(message)
        {

        }
    }
}
