﻿using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Logging.Abstractions;
using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using System.Text;

namespace Wheel.Ftp
{
    /// <summary>
    /// Provide high level static operations, and an access to the library LoggerFactory.
    /// </summary>
    public static class FTPOperation
    {
        #region Logger factory

        private static ILoggerFactory _loggerFactory;

        public static bool HasLoggerFactory => _loggerFactory != null;

        public static ILoggerFactory LoggerFactory
        {
            get
            {
                if (_loggerFactory == null)
                {
                    Console.Error.WriteLine("No logger factory configured for Wheel.Ftp (property FTPOperation.LoggerFactory)");
                    _loggerFactory = NullLoggerFactory.Instance;
                }
                return _loggerFactory;
            }
            set
            {
                _loggerFactory = value;
            }
        }

        #endregion

        #region Upload
        
        public static void SafeTransfer(string file, string server, string username, string password, int port = 21, string remoteDir = null, int retries = 30)
        {
            if (port == default(int))
                port = 21;
            if (file == null)
                throw new ArgumentNullException(nameof(file));
            if (server == null)
                throw new ArgumentNullException(nameof(server));
            if (username == null)
                throw new ArgumentNullException(nameof(username));
            if (password == null)
                throw new ArgumentNullException(nameof(password));

            using (var ftp = new FTPConnection
            {
                ServerAddress = server,
                ServerPort = port,
                ConnectMode = FTPConnectMode.PASV,
                AutoLogin = true,
                UserName = username,
                Password = password
            })
            {
                // Note: no call to ftp.Connect() here: it will be handled within the retry loop in SafeTransfer() method.
                ftp.SafeTransfer(file, remoteDir, retries);
            }
        }

        #endregion
    }
}
