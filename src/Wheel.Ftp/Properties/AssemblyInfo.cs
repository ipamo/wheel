﻿using System.Runtime.CompilerServices;

[assembly: InternalsVisibleTo("Wheel.Testing")]
[assembly: InternalsVisibleTo("Wheel.Tests")]
