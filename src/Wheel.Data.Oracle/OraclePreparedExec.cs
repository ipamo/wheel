﻿using Microsoft.Extensions.Logging;
using Oracle.ManagedDataAccess.Client;
using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Text;

namespace Wheel.Data
{
    internal sealed class OraclePreparedExec : IPreparedExec
    {
        public OracleConnection Connection { get; }

        public ILogger Logger { get; }

        public string LogPrefix { get; }

        public bool CatchExceptions { get; }

        public OraclePreparedExec(OracleConnection connection, ILogger logger, string logPrefix, bool catchExceptions)
        {
            Connection = connection;
            Logger = logger;
            LogPrefix = logPrefix;
            CatchExceptions = catchExceptions;
        }
    }
}
