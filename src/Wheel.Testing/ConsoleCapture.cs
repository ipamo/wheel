﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using Xunit.Abstractions;

namespace Wheel.Testing
{
    /// <summary>
    /// A utility for capturing standard output and error streams.
    /// </summary>
    /// <remarks>
    /// To ensure correct capture in multiple tests, it is usually necessary
    /// to turn off xunit parallelism inside the assembly. In Properties/AssemblyInfo.cs:
    /// <code>
    /// using Xunit;
    /// 
    /// // Turn off parallelism inside the assembly
    /// // Usefull for tests based on Wheel.Testing.ConsoleCapture.
    /// [assembly: CollectionBehavior(DisableTestParallelization = true)]
    /// </code>
    /// </remarks>
    public class ConsoleCapture : IDisposable
    {
        private readonly TextWriter _initOut;
        private readonly TextWriter _initErr;

        private readonly StringWriter _tmpOut;
        private readonly StringWriter _tmpErr;
        private readonly ITestOutputHelper _output;

        public ConsoleCapture(ITestOutputHelper output = null)
        {
            _initOut = Console.Out;
            _initErr = Console.Error;

            _tmpOut = new StringWriter();
            _tmpErr = new StringWriter();
            _output = output;

            Console.SetOut(_tmpOut);
            Console.SetError(_tmpErr);
        }

        public void Dispose()
        {
            Console.SetOut(_initOut);
            Console.SetError(_initErr);

            string str;

            _tmpErr.Flush();
            str = GetError();
            if (! string.IsNullOrWhiteSpace(str))
            {
                _output.WriteLine("-----BEGIN CAPTURED STANDARD ERROR-----");
                _output.WriteLine(str);
                _output.WriteLine("-----END CAPTURED STANDARD ERROR-----");
            }

            _tmpOut.Flush();
            str = GetOut();
            if (!string.IsNullOrWhiteSpace(str))
            {
                _output.WriteLine("-----BEGIN CAPTURED STANDARD OUTPUT-----");
                _output.WriteLine(str);
                _output.WriteLine("-----END CAPTURED STANDARD OUTPUT-----");
            }
        }

        public string GetOut()
            => _tmpOut.GetStringBuilder().ToString();

        public string GetError()
            => _tmpErr.GetStringBuilder().ToString();
    }
}
