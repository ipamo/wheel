﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Wheel.Data
{
    public static class PostgreSqlUtils
    {

        /// <summary>
        /// Ensure all sequences for "Id" properties are initialized
        /// and that next value will be at least <paramref name="minNext"/>.
        /// </summary>
        /// <remarks>
        /// This DOES NOT ensure that the next value will be greater than the current max value of the column.
        /// For this, use <see cref="SynchronizeSequence"/>.
        /// </remarks>
        public static void SynchronizeIdSequences(this IDbms dbms, int minNext = 1)
        {
            var sql = $@"
SELECT
    seqname,
    setval(seqname,
        CASE WHEN nextval(seqname) > {minNext} THEN currval(seqname) - 1 ELSE {minNext} - 1 END,
        TRUE) + 1 AS seqnext
FROM
	(SELECT concat(n.nspname, '.', c.relname) AS seqname
	FROM pg_class c
	INNER JOIN pg_namespace n ON n.oid = c.relnamespace
	WHERE c.relkind = 'S' AND c.relname LIKE '%_id_seq'
	) s
;";

            dbms.NonQuery(sql);
        }

        /// <summary>
        /// Ensure the sequence for the given column is initialized
        /// and that next value will be at least <paramref name="minNext"/>.
        /// </summary>
        /// <remarks>
        /// This also ensure that the next value will be greater than
        /// the current max value of the column.
        /// </remarks>
        public static void SynchronizeSequence<TEntity>(this IDbms dbms, string column = "id", int minNext = 1)
            where TEntity : class
        {
            var schema = dbms.GetSchemaName(typeof(TEntity));
            var table = dbms.GetTableName(typeof(TEntity));
            var seqname = $"{schema}.{table}_{column}_seq";

            var sql = $@"
SELECT
    seqname,
    setval(seqname,
        CASE WHEN nextval(seqname) > minNextSafe THEN currval(seqname) - 1 ELSE minNextSafe - 1 END,
        TRUE) + 1 AS seqnext,
    columnmax
FROM
    (SELECT
        '{seqname}' AS seqname,
        MAX(""{ column}"") AS columnmax,
        CASE WHEN MAX(""{ column}"") < {minNext} THEN {minNext} ELSE MAX(""{ column}"") + 1 END AS minNextSafe
    FROM ""{schema}"".""{table}""
    ) s
";

            dbms.NonQuery(sql);
        }
    }
}
