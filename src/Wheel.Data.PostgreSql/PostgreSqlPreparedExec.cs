﻿using Microsoft.Extensions.Logging;
using Npgsql;
using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Text;

namespace Wheel.Data
{
    internal sealed class PostgreSqlPreparedExec : IPreparedExec
    {
        public NpgsqlConnection Connection { get; }

        public ILogger Logger { get; }

        public string LogPrefix { get; }

        public bool CatchExceptions { get; }

        public PostgreSqlPreparedExec(NpgsqlConnection connection, ILogger logger, string logPrefix, bool catchExceptions)
        {
            Connection = connection;
            Logger = logger;
            LogPrefix = logPrefix;
            CatchExceptions = catchExceptions;
        }
    }
}
