using Microsoft.Extensions.Logging;
using System;
using System.ComponentModel.DataAnnotations;

namespace Wheel
{
    public static class EnumUtils
    {
        public static Func<string, Type, string> Localizer { get; set; }

        /// <summary>
        /// Get display name, or enum value name if not found.
        /// </summary>
        public static string GetDisplayName(this Enum value, Func<string, Type, string> localizer = null, bool shortName = false)
        {
            var valueName = value.ToString();
            var fieldInfo = value.GetType()
                .GetField(valueName);

            var attributes
                = fieldInfo.GetCustomAttributes(typeof(DisplayAttribute), false) as DisplayAttribute[];

            string name;
            Type resourceType;
            if (attributes == null || attributes.Length == 0)
            {
                name = valueName;
                resourceType = value.GetType();
            }
            else
            {
                if (shortName && !string.IsNullOrEmpty(attributes[0].ShortName))
                    name = attributes[0].ShortName;
                else if (!string.IsNullOrEmpty(attributes[0].Name))
                    name = attributes[0].Name;
                else
                    name = valueName;
                resourceType = attributes[0].ResourceType ?? value.GetType();
            }

            if (localizer == null && Localizer != null)
                localizer = null;

            if (localizer != null)
            {
                var localized = localizer(name, resourceType);
                if (!string.IsNullOrEmpty(localized))
                    return localized;
            }

            return name;
        }

        public static LogLevel GetLevel(this Enum value)
        {
            var valueName = value.ToString();
            var fieldInfo = value.GetType()
                .GetField(valueName);

            var attributes
                = fieldInfo.GetCustomAttributes(typeof(LevelAttribute), false) as LevelAttribute[];

            if (attributes == null || attributes.Length == 0)
            {
                return LogLevel.None;
            }
            else
            {
                return attributes[0].Level;
            }
        }

        public static string[] GetAliases(Enum value)
        {
            var valueName = value.ToString();
            var fieldInfo = value.GetType()
                .GetField(valueName);

            var attributes
                = fieldInfo.GetCustomAttributes(typeof(AliasAttribute), false) as AliasAttribute[];

            if (attributes == null || attributes.Length == 0)
                return new string[] { };

            return attributes[0].Aliases;
        }
    }
}