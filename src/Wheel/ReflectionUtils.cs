﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Text;

namespace Wheel
{
    /// <summary>
    /// Helper methods related to reflection.
    /// </summary>
    public static class ReflectionUtils
    {
        /// <summary>
        /// Determine the application assembly (using stack trace is necessary).
        /// </summary>
        public static Assembly GetApplicationAssembly()
        {
            var assembly = Assembly.GetEntryAssembly();
            var assemblyName = assembly?.GetName().Name;

            if (assemblyName != null && !(assemblyName.Equals("System.Private.CoreLib")
                    || assemblyName.Equals("mscorlib", StringComparison.OrdinalIgnoreCase)
                    || assemblyName.Equals("testhost", StringComparison.OrdinalIgnoreCase)
                    || assemblyName.StartsWith("xunit", StringComparison.OrdinalIgnoreCase)
                    || assemblyName.StartsWith("ef", StringComparison.OrdinalIgnoreCase)
                    || assemblyName.StartsWith("Microsoft.", StringComparison.OrdinalIgnoreCase)
                ))
                return assembly;

            // On cherche dans la StackTrace
            var st = new StackTrace();
            Assembly lastAssembly = null;
            foreach (var frame in st.GetFrames())
            {
                assembly = frame.GetMethod().DeclaringType.Assembly;
                assemblyName = assembly.GetName().Name;
                if (assemblyName.Equals("System.Private.CoreLib")
                    || assemblyName.Equals("mscorlib", StringComparison.OrdinalIgnoreCase)
                    || assemblyName.Equals("testhost", StringComparison.OrdinalIgnoreCase)
                    || assemblyName.StartsWith("xunit", StringComparison.OrdinalIgnoreCase)
                    || assemblyName.StartsWith("ef", StringComparison.OrdinalIgnoreCase)
                    || assemblyName.StartsWith("Microsoft.", StringComparison.OrdinalIgnoreCase)
                )
                {
                    break;
                }
                lastAssembly = assembly;
            }

            if (lastAssembly == null)
                throw new ApplicationException("Application assembly not found");

            return lastAssembly;
        }

        public static string GetAssemblyVersion(Assembly assembly)
        {
            var attrs = assembly.GetCustomAttributes(typeof(AssemblyInformationalVersionAttribute), false);
            if (attrs != null && attrs.Length > 0)
            {
                var version = ((AssemblyInformationalVersionAttribute)attrs[0]).InformationalVersion?.Trim();
                if (!string.IsNullOrEmpty(version))
                    return version;
            }

            attrs = assembly.GetCustomAttributes(typeof(AssemblyVersionAttribute), false);
            if (attrs != null && attrs.Length > 0)
            {

                var version = ((AssemblyVersionAttribute)attrs[0]).Version?.Trim();
                if (!string.IsNullOrEmpty(version))
                    return version;
            }

            attrs = assembly.GetCustomAttributes(typeof(AssemblyFileVersionAttribute), false);
            if (attrs != null && attrs.Length > 0)
            {
                var version = ((AssemblyFileVersionAttribute)attrs[0]).Version?.Trim();
                if (!string.IsNullOrEmpty(version))
                    return version;
            }

            return null;
        }

        public static string GetAssemblyDescription(Assembly assembly)
        {
            var attrs = assembly.GetCustomAttributes(typeof(AssemblyDescriptionAttribute), false);
            if (attrs != null && attrs.Length > 0)
            {
                var description = ((AssemblyDescriptionAttribute)attrs[0]).Description?.Trim();
                if (description == "Package Description")
                    description = null;
                if (!string.IsNullOrEmpty(description))
                    return description;
            }
                        
            return null;
        }

        /// <summary>
        /// Return the assembly configuration: Release or Debug.
        /// </summary>
        public static string GetAssemblyConfiguration(Assembly assembly)
        {
            var attrs = assembly.GetCustomAttributes(typeof(AssemblyConfigurationAttribute), false);
            if (attrs != null && attrs.Length > 0)
            {
                var configuration = ((AssemblyConfigurationAttribute)attrs[0]).Configuration?.Trim();
                if (!string.IsNullOrEmpty(configuration))
                    return configuration;
            }

            return null;
        }


        public static int GetFrameCount(this StackTrace strackTrace)
        {
#if NET20 || NET461
            return strackTrace.FrameCount;
#else
            return strackTrace.GetFrames().Length;
#endif
        }

        /// <summary>
        /// Gets the fully qualified name of the class invoking the calling method, including the namespace but not the assembly.    
        /// </summary>
        [MethodImpl(MethodImplOptions.NoInlining)]
        public static string GetClassFullName()
        {
            int framesToSkip = 2;

            string className = string.Empty;
#if NET20 || NET461
            Type declaringType;

            do
            {
#if SILVERLIGHT
                StackFrame frame = new StackTrace().GetFrame(framesToSkip);
#else
                StackFrame frame = new StackFrame(framesToSkip, false);
#endif
                MethodBase method = frame.GetMethod();
                declaringType = method.DeclaringType;
                if (declaringType == null)
                {
                    className = method.Name;
                    break;
                }

                framesToSkip++;
                className = declaringType.FullName;
            } while (className.StartsWith("System.", StringComparison.Ordinal));
#else
            var stackTrace = Environment.StackTrace;
            var stackTraceLines = stackTrace.Replace("\r", "").Split(new[] { "\n" }, StringSplitOptions.RemoveEmptyEntries);
            for (int i = 0; i < stackTraceLines.Length; ++i)
            {
                var callingClassAndMethod = stackTraceLines[i].Split(new[] { " ", "<>", "(", ")" }, StringSplitOptions.RemoveEmptyEntries)[1];
                int methodStartIndex = callingClassAndMethod.LastIndexOf(".", StringComparison.Ordinal);
                if (methodStartIndex > 0)
                {
                    // Trim method name. 
                    var callingClass = callingClassAndMethod.Substring(0, methodStartIndex);
                    // Needed because of extra dot, for example if method was .ctor()
                    className = callingClass.TrimEnd('.');
                    if (!className.StartsWith("System.Environment") && framesToSkip != 0)
                    {
                        i += framesToSkip - 1;
                        framesToSkip = 0;
                        continue;
                    }
                    if (!className.StartsWith("System."))
                        break;
                }
            }
#endif
            return className;
        }

        /// <summary>
        /// Return a dump of all assemblies referenced by all loaded assemblies.
        /// </summary>
        public static string DumpReferencedAssemblies()
        {
            var writer = new StringWriter();
            DumpReferencedAssemblies(writer);
            return writer.ToString();
        }

        /// <summary>
        /// Return a dump of all assemblies referenced by the given assembly.
        /// </summary>
        public static string DumpReferencedAssemblies(Assembly assembly)
        {
            var writer = new StringWriter();
            DumpReferencedAssemblies(writer, assembly);
            return writer.ToString();
        }

        /// <summary>
        /// Return a dump of all assemblies referenced by all loaded assemblies.
        /// </summary>
        public static void DumpReferencedAssemblies(TextWriter writer)
        {
            foreach (var assembly in AppDomain.CurrentDomain.GetAssemblies().OrderBy(n => n.GetName().Name))
            {
                DumpReferencedAssemblies(writer, assembly);
            }
        }

        /// <summary>
        /// Return a dump of all assemblies referenced by the given assembly.
        /// </summary>
        public static void DumpReferencedAssemblies(TextWriter writer, Assembly assembly)
        {
            var loaded = AppDomain.CurrentDomain.GetAssemblies().Select(x => x.GetName());

            int indent = 0;

            // Given assembly
            {
                var isLoaded = loaded.Any(a => a.Name == assembly.GetName().Name);

                var text = "";
                for (var i = 0; i < indent; i++)
                    text += " ";

                text += "- ";
                if (isLoaded)
                    text += "[L] ";

                text += assembly.GetName().Name + " (" + assembly.GetName().Version + ")";

                writer.WriteLine(text);
            }

            indent += 2;
            
            // Referenced by given assembly
            foreach (var referenced in assembly.GetReferencedAssemblies().OrderBy(n => n.Name))
            {
                var isLoaded = loaded.Any(a => a.Name == referenced.Name);

                var text = "";
                for (var i = 0; i < indent; i++)
                    text += " ";

                text += "- ";
                if (isLoaded)
                    text += "[L] ";

                text += referenced.Name + " (" + referenced.Version + ")";

                writer.WriteLine(text);
            }
        }
        
        public static object CreateInstance(Type instanceType, IDictionary<Type, object> availableParams, IServiceProvider services = null)
        {
            var constructors = instanceType
#if !NET20
                .GetTypeInfo()
#endif
                .GetConstructors();

            var exceptionPrefix = string.Format("Cannot instanciate {0}", instanceType);

            if (constructors.Length == 0)
                throw new MessageException($"{exceptionPrefix}: no constructor");
            else if (constructors.Length > 1)
                throw new MessageException($"{exceptionPrefix}: several constructors");

            var parameters = PrepareParametersFromAvailable(constructors[0].GetParameters(), availableParams, services, exceptionPrefix);

            try
            {
                return Activator.CreateInstance(instanceType, parameters);
            }
            catch (TargetInvocationException ex) when (ex.InnerException != null)
            {
                var e = ex.InnerException;
                var message = string.Format("{0} raised in the constructor of {1}: {2}", e.GetType().Name, instanceType, e.Message);
                throw new MessageException(message, e);
            }
            catch (Exception e)
            {
                throw new MessageException($"{exceptionPrefix}: {e.Message}", e);
            }
        }

        public static T CreateInstance<T>(IDictionary<Type, object> availableParams, IServiceProvider services = null)
            => (T)CreateInstance(typeof(T), availableParams, services);

        public static object[] PrepareParameters(MethodInfo method, IDictionary<Type, object> availableParams, IServiceProvider services = null)
        {
            var requestedParams = method.GetParameters();
            return PrepareParametersFromAvailable(requestedParams, availableParams, services, string.Format("Method {0} in {1}", method.Name, method.DeclaringType));
        }

        public static object[] PrepareParameters(MethodInfo method, IServiceProvider services, params object[] paramsArray)
        {
            var requestedParams = method.GetParameters();
            return PrepareParametersFromArrayOrder(requestedParams, paramsArray, services, string.Format("Method {0} in {1}", method.Name, method.DeclaringType));
        }

        private static object[] PrepareParametersFromAvailable(ParameterInfo[] paramList, IDictionary<Type, object> availableParams, IServiceProvider services, string exceptionPrefix)
        {
            var parameterValues = new object[paramList.Length];
            var unknownParameters = new List<string>(parameterValues.Length);
            for (int i = 0; i < parameterValues.Length; i++)
            {
                var type = paramList[i].ParameterType;
                Type typeArg = null;

                // example: transform ILogger<T> to ILogger<> and keep T as the type argument
                if (type.IsGenericType) 
                {
                    var args = type.GetGenericArguments();
                    if (args.Length >= 1)
                    {
                        typeArg = args[0];
                        type = type.GetGenericTypeDefinition();
                    }
                }

                object service;
                if (availableParams.ContainsKey(type))
                {
                    object value = availableParams[type];
                    var valueType = value.GetType();
                    if (valueType.IsGenericType)
                    {
                        var valueGenericType = valueType.GetGenericTypeDefinition();
                        // Resolve the param if it is a Func<> object
                        try
                        {
                            // Func<object>
                            if (valueGenericType == typeof(Func<>))
                            {
                                parameterValues[i] = ((Delegate)value).DynamicInvoke();
                                continue;
                            }
                            // Func<Type,object> with a typeArg
                            else if (typeArg != null && valueGenericType == typeof(Func<,>))
                            {

                                parameterValues[i] = ((Delegate)value).DynamicInvoke(typeArg);
                                continue;
                            }
                        }
                        catch (Exception ex) when (ex.InnerException != null)
                        {
                            var e = ex.InnerException;
                            var message = string.Format("{0}: {1} raised while activating parameter of type {2}: {3}", exceptionPrefix, e.GetType().Name, type, e.Message);
                            throw new MessageException(message, e);
                        }
                    }
                    parameterValues[i] = value;
                }
                else if (services != null && (service = services.GetService(paramList[i].ParameterType)) != null)
                {
                    parameterValues[i] = service;
                }
                else
                {
                    unknownParameters.Add(paramList[i].ParameterType.Name);
                }
            }

            if (unknownParameters.Any())
            {
                var types = unknownParameters.Count > 1 ? "types " + string.Join(", ", unknownParameters.ToArray()) : "type " + unknownParameters[0];
                throw new MessageException($"{exceptionPrefix}: parameter {types} not available");
            }

            return parameterValues;
        }

        private static object[] PrepareParametersFromArrayOrder(ParameterInfo[] paramList, object[] paramsArray, IServiceProvider services, string exceptionPrefix)
        {
            var parameterValues = new object[paramList.Length];
            var unknownParameters = new List<string>(parameterValues.Length);
            for (int i = 0; i < parameterValues.Length; i++)
            {
                object service;
                if (i < paramsArray.Length)
                {
                    parameterValues[i] = paramsArray[i];
                }
                else if (services != null && (service = services.GetService(paramList[i].ParameterType)) != null)
                {
                    parameterValues[i] = service;
                }
                else
                {
                    unknownParameters.Add(paramList[i].ParameterType.Name);
                }
            }

            if (unknownParameters.Any())
            {
                var types = unknownParameters.Count > 1 ? "types " + string.Join(", ", unknownParameters.ToArray()) : "type " + unknownParameters[0];
                throw new MessageException($"{exceptionPrefix}: parameter {types} not available");
            }

            return parameterValues;
        }
    }
}
