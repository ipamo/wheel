﻿using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Text;

namespace Wheel
{
    public static class ExceptionUtils
    {
        public static bool CanHandle(Exception e)
        {
            return e is MessageException;
        }

        public static int Handle(Exception e, ILogger logger = null)
        {
            if (e is MessageException ex)
            {
                LoggingUtils.SafeLog(logger, LogLevel.Critical, ex.Message);
            }
            else
            {
                throw e;
            }

            return 1;
        }

        public static int Handle(Exception e, IServiceProvider services)
        {
            ILogger logger = null;
            if (services != null)
            {
                var loggerFactory = services.GetService(typeof(ILoggerFactory)) as ILoggerFactory;
                if (loggerFactory != null)
                    logger = loggerFactory.CreateLogger(typeof(ExceptionUtils));
            }
            return Handle(e, logger);
        }
    }
}
