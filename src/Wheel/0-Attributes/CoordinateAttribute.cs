﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Wheel
{
    /// <summary>
    /// Specify that a column holds geographical coordinates (latitude, longitude).
    /// </summary>
    /// <remarks>
    /// Should imply column type "decimal(10,7)".
    /// </remarks>
    [AttributeUsage(AttributeTargets.Property)]
    public class CoordinateAttribute : DecimalAttribute
    {
        public CoordinateAttribute(int precision = 10, int scale = 7)
            : base(precision, scale)
        {

        }
    }
}
