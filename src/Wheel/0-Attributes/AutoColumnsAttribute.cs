﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Wheel
{
    /// <summary>
    /// Add auto-columns (CreatedAt, UpdatedAt, etc).
    /// </summary>
    [AttributeUsage(AttributeTargets.Class, AllowMultiple = false, Inherited = false)]
    public class AutoColumnsAttribute : Attribute
    {

    }
}
