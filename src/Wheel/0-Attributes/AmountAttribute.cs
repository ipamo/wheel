﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Wheel
{
    /// <summary>
    /// Specify that a column holds monetary amount.
    /// </summary>
    /// <remarks>
    /// Should imply column type "decimal(13,4)".
    /// See GAAP: https://rietta.com/blog/best-data-types-for-currencymoney-in
    /// See SEC's NMS Rule 612: minimum pricing increment is $0.0001.
    /// </remarks>
    [AttributeUsage(AttributeTargets.Property)]
    public class AmountAttribute : DecimalAttribute
    {
        public AmountAttribute(int precision = 13, int scale = 4)
            : base(precision, scale)
        {

        }
    }
}
