﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace Wheel
{
    /// <summary>
    /// Conversion from and to hexadecimal notation.
    /// </summary>
    public static class HexUtils
    {
        /// <summary>
        /// Indicate whether a character is hexadecimal.
        /// </summary>
        public static bool IsHex(char c)
        {
            return (c >= '0' && c <= '9') ||
                (c >= 'a' && c <= 'f') ||
                (c >= 'A' && c <= 'F');
        }


        /// <summary>
        /// Convert a byte array into a hexadecimal string.
        /// </summary>
        /// <param name="bytes">Byte array</param>
        /// <param name="start">Start index</param>
        /// <param name="length">Length</param>
        /// <param name="group">Separate hexadecimal chars into groups of the specified size.
        /// If zero, no separation is performed.</param>
        /// <param name="separator">Separator character</param>
        /// <returns></returns>
        public static string ToString(byte[] bytes, int start = 0, int length = -1, int group = 0, char separator = '-')
        {
            if (bytes == null)
                return null;
            if (length < 0)
                length = bytes.Length;

            if (IsHex(separator))
                throw new InvalidOperationException("Le caractère séparateur d'une chaîne hexadécimale ne doit pas être lui-même un caractère hexadécimal");

            // La méthode ci-dessous fournit une représentation avec tailleGroupes=2 et separateur=-
            string defaultHex = BitConverter.ToString(bytes, start, length).ToLowerInvariant();

            if (group == 2 && separator == '-')
            {
                return defaultHex;
            }
            else
            {
                // On procède à un regroupement différent de celui de defaultHex
                StringBuilder sb = new StringBuilder();

                int n = 0;
                foreach (char c in defaultHex)
                {
                    if (!IsHex(c))
                        continue;

                    n++;

                    if (group <= 0)
                    {
                        sb.Append(c);
                    }
                    else
                    {
                        sb.Append(c);
                        if (n % group == 0)
                            sb.Append(separator);
                    }
                }

                // Suppression du dernier caractère s'il s'agit du séparateur
                if (n > 0 && group > 0 && n % group == 0)
                {
                    sb.Remove(sb.Length - 1, 1);
                }

                return sb.ToString();
            }
        }

        /// <summary>
        /// Convert a hexadecimal string into a byte array.
        /// </summary>
        public static byte[] FromString(string str)
        {
            // Remove all non-hexadecimal chars from the string
            StringBuilder sb = new StringBuilder();
            foreach (char c in str)
            {
                if (IsHex(c))
                    sb.Append(c);
            }

            str = sb.ToString();
            if (str.Length % 2 == 1)
                str = "0" + str;

            // Convert string to byte array
            int countBytes = str.Length / 2;
            byte[] bytes = new byte[countBytes];
            for (int i = 0; i < countBytes; i++)
                bytes[i] = Convert.ToByte(str.Substring(2 * i, 2), 16);
            return bytes;
        }
    }
}
