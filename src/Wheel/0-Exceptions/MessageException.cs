﻿using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Wheel
{
    /// <summary>
    /// Exception used when the message is enough to debug: stack trace does not need to be printed during application termination.
    /// </summary>
    public class MessageException : Exception
    {
        public LogLevel Level { get; set; }
        public EventId EventId { get; set; }

        public MessageException(string message, Exception innerException = null, LogLevel level = LogLevel.Error, EventId eventId = default(EventId))
            : base(message, innerException)
        {
            Level = level;
            EventId = eventId;
        }
    }
}
