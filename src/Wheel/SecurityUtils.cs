﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using System.Security;
using System.Security.Cryptography;
using System.Text;
using System.Text.RegularExpressions;

namespace Wheel
{
    public static class SecurityUtils
    {
        private readonly static char[] AlphanumericChars =
            "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890".ToCharArray();
        private readonly static char[] LowerCaseAlphanumericChars =
            "abcdefghijklmnopqrstuvwxyz1234567890".ToCharArray();

        public static string GenerateAlphanumericString(int length = 32, bool lowerCase = false)
        {
            byte[] data = new byte[4 * length];

#if NET20
            var crypto = new RNGCryptoServiceProvider();
            crypto.GetBytes(data);
#else
            using (var crypto = new RNGCryptoServiceProvider())
            {
                crypto.GetBytes(data);
            }
#endif

            var result = new StringBuilder(length);
            for (int i = 0; i < length; i++)
            {
                var rnd = BitConverter.ToUInt32(data, i * 4);

                if (lowerCase)
                {
                    var idx = rnd % LowerCaseAlphanumericChars.Length;
                    result.Append(LowerCaseAlphanumericChars[idx]);
                }
                else
                {
                    var idx = rnd % AlphanumericChars.Length;
                    result.Append(AlphanumericChars[idx]);
                }
            }

            return result.ToString();
        }

        /// <summary>
        /// Read password from consolde input.
        /// </summary>
        /// <param name="asterisks">If true, an asterisk is displayed for each character. If false, nothing is displayed.</param>
        /// <returns></returns>
        public static string PromptPassword(bool asterisks = true)
        {
            ConsoleKeyInfo key;
            var sb = new StringBuilder();

            do
            {
                key = Console.ReadKey(true);

                if (key.Key != ConsoleKey.Backspace && key.Key != ConsoleKey.Enter)
                {
                    sb.Append(key.KeyChar);
                    if (asterisks)
                        Console.Write("*");
                }
                else
                {
                    if (key.Key == ConsoleKey.Backspace && sb.Length > 0)
                    {
                        sb.Remove(sb.Length - 1, 1);
                        if (asterisks)
                            Console.Write("\b \b");
                    }
                }
            }
            while (key.Key != ConsoleKey.Enter);

            Console.WriteLine();
            return sb.ToString();
        }


        /// <summary>
        /// Read password from consolde input.
        /// </summary>
        /// <param name="asterisks">If true, an asterisk is displayed for each character. If false, nothing is displayed.</param>
        /// <returns></returns>
        public static SecureString PromptSecureStringPassword(bool asterisks = true)
        {
            ConsoleKeyInfo key;
            var pwd = new SecureString();

            do
            {
                key = Console.ReadKey(true);

                if (key.Key != ConsoleKey.Backspace && key.Key != ConsoleKey.Enter)
                {
                    pwd.AppendChar(key.KeyChar);
                    if (asterisks)
                        Console.Write("*");
                }
                else
                {
                    if (key.Key == ConsoleKey.Backspace && pwd.Length > 0)
                    {
                        pwd.RemoveAt(pwd.Length - 1);
                        if (asterisks)
                            Console.Write("\b \b");
                    }
                }
            }
            while (key.Key != ConsoleKey.Enter);

            Console.WriteLine();
            return pwd;
        }

        public static string SecureStringToString(SecureString value)
        {
            var valuePtr = IntPtr.Zero;
            try
            {
                valuePtr = Marshal.SecureStringToGlobalAllocUnicode(value);
                return Marshal.PtrToStringUni(valuePtr);
            }
            finally
            {
                Marshal.ZeroFreeGlobalAllocUnicode(valuePtr);
            }
        }

        /// <summary>
        /// Return the given connection string with hidden password
        /// </summary>
        public static string HidePassword(string str)
        {
            return Regex.Replace(str, @"(.*;)?Password=([^;]+)(;.*)?", "$1Password=<hidden>$3");
        }
    }
}
