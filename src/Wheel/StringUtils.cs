﻿using System;
using System.Collections.Generic;
using System.Linq;
#if !NET20
using System.Linq.Expressions;
#endif
using System.Text;
using System.Text.RegularExpressions;
using System.Web;

namespace Wheel
{
    /// <summary>
    /// Helper methods related to strings.
    /// </summary>
    public static class StringUtils
    {
        // See https://stackoverflow.com/questions/298830/split-string-containing-command-line-parameters-into-string-in-c-sharp/298990#298990
        public static string[] SplitCommandLine(string commandLine)
        {
            bool inQuotes = false;

            return SplitString(commandLine, c =>
            {
                if (c == '\"')
                    inQuotes = !inQuotes;

                return !inQuotes && c == ' ';
            })
                              .Select(arg => TrimMatchingQuotes(arg.Trim(), '\"'))
                              .Where(arg => !string.IsNullOrEmpty(arg))
                              .ToArray();
        }

        // See https://stackoverflow.com/questions/298830/split-string-containing-command-line-parameters-into-string-in-c-sharp/298990#298990
        public static IEnumerable<string> SplitString(string str,
                                            Func<char, bool> controller)
        {
            int nextPiece = 0;

            for (int c = 0; c < str.Length; c++)
            {
                if (controller(str[c]))
                {
                    yield return str.Substring(nextPiece, c - nextPiece);
                    nextPiece = c + 1;
                }
            }

            yield return str.Substring(nextPiece);
        }

        // See https://stackoverflow.com/questions/298830/split-string-containing-command-line-parameters-into-string-in-c-sharp/298990#298990
        public static string TrimMatchingQuotes(string input, char quote)
        {
            if ((input.Length >= 2) &&
                (input[0] == quote) && (input[input.Length - 1] == quote))
                return input.Substring(1, input.Length - 2);

            return input;
        }

        public static string ToLowerSnake(this string str, char separator = '_')
        {
            if (str == null)
                return null;

            var sb = new StringBuilder();
            bool addSeparatorIfUpper = false;
            for (var i = 0; i < str.Length; i++)
            {
                var c = str[i];
                if (char.IsUpper(c))
                {
                    if (addSeparatorIfUpper)
                        sb.Append(separator);
                    sb.Append(char.ToLower(c));
                    addSeparatorIfUpper = false;
                }
                else
                {
                    sb.Append(c);
                    addSeparatorIfUpper = char.IsLower(c) || char.IsDigit(c);
                }
            }
            return sb.ToString();
        }

        public static string ToUpperSnake(this string str, char separator = '_')
        {
            if (str == null)
                return null;

            var sb = new StringBuilder();
            bool addSeparatorIfUpper = false;
            for (var i = 0; i < str.Length; i++)
            {
                var c = str[i];
                if (char.IsUpper(c))
                {
                    if (addSeparatorIfUpper)
                        sb.Append(separator);
                    sb.Append(c);
                    addSeparatorIfUpper = false;
                }
                else
                {
                    sb.Append(char.ToUpper(c));
                    addSeparatorIfUpper = char.IsLower(c);
                }
            }
            return sb.ToString();
        }




        public static string StartsAfter(this string str, string prefix)
        {
            if (str == null)
                return null;
            if (prefix == null)
                return str;

            var pos = str.IndexOf(prefix);
            if (pos < 0)
                return null;
            pos += prefix.Length;
            return str.Substring(pos);
        }



        public static string TrimOrNull(this string str)
        {
            if (str == null)
                return null;

            str = str.Trim();
            if (str == "")
                return null;

            return str;
        }

#if !NET20
        /// <summary>
        /// Apply <see cref="TrimOrNull"/> on the given property and return the trimmed value.
        /// </summary>
        public static string ApplyTrimOrNull<TModel>(TModel model, Expression<Func<TModel, string>> expression)
        {
            var prop = ExpressionUtils.GetExpressionProperty(expression);
            var value = (string)prop.GetValue(model);
            var trimmedValue = value.TrimOrNull();
            prop.SetValue(model, trimmedValue);
            return trimmedValue;
        }

        /// <summary>
        /// Apply <see cref="TrimOrNull"/> on all given properties.
        /// </summary>
        public static Expression<Func<TModel, string>>[] ApplyTrimOrNull<TModel>(TModel model, params Expression<Func<TModel, string>>[] expressions)
        {
            if (expressions != null)
            {
                foreach (var expression in expressions)
                {
                    ApplyTrimOrNull(model, expression);
                }
            }

            return expressions;
        }
#endif

#if !NET20 && !NET461
        /// <summary>
        /// Encode for Javascript.
        /// </summary>
        public static string JsEncode(string value, bool addDoubleQuotes = false)
        {
            return HttpUtility.JavaScriptStringEncode(value, addDoubleQuotes);
        }
#endif

        /// <summary>
        /// Format a string so that it can be used as CSV (French version).
        /// </summary>
        public static string FormatForCSV_FR(string raw)
        {
            if (raw == null)
                return null;

            bool needEscape = raw.Contains("\n") || raw.Contains("\r") || raw.Contains(";") || raw.Contains("\"");

            return (needEscape ? "\"" : "") + raw.Replace("\"", "\"\"") + (needEscape ? "\"" : "");
        }

        private static readonly Regex ISO_FORMAT = new Regex(@"^(\d{2,4})-(\d{1,2})-(\d{1,2})$");
        private static readonly Regex NUMERIC_FORMAT = new Regex(@"^(\d{4})(\d{2})(\d{2})$");
        private static readonly Regex FRANCE_FORMAT = new Regex(@"^(\d{1,2})/(\d{1,2})/(\d{2,4})$");

        /// <summary>
        /// Convert a list of string into a list of dates.
        /// <para>
        /// 20180101 20180109 20180117 are considered as 3 dates.
        /// 20180101_20180109 20180117 are a range: from January 1 to January 9, and January 17.
        /// Dates can be expressed either as yyyyMMdd, yyyy-MM-dd or dd/MM/yyyy
        /// </para>
        /// </summary>
        public static List<DateTime> ToDates(IEnumerable<string> dateString, bool defaultToday = false)
        {
            // Default
            if (dateString == null || !dateString.Any())
            {
                if (defaultToday)
                    return new List<DateTime> { DateTime.Today };
                else
                    return new List<DateTime>();
            }

            // Cut arguments into list of dates and separators
            var argList = new List<string>();
            foreach (var rawarg in dateString)
            {
                int index;
                var arg = rawarg.Trim();
                if (arg == "_")
                {
                    argList.Add("_");
                }
                else if ((index = arg.IndexOf("_")) >= 0)
                {
                    if (index > 0)
                        argList.Add(arg.Substring(0, index));
                    argList.Add("_");
                    if (index < arg.Length - 1)
                        argList.Add(arg.Substring(index + 1));
                }
                else
                {
                    argList.Add(arg);
                }
            }


            var dates = new List<DateTime>();
            DateTime? prevDate = null;
            bool prevIsRange = false;

            foreach (var arg in argList)
            {
                if (arg == "_")
                {
                    prevIsRange = true;
                    continue;
                }

                Match m;
                int year;
                int month;
                int day;
                if ((m = ISO_FORMAT.Match(arg)).Success)
                {
                    year = int.Parse(m.Groups[1].Value);
                    month = int.Parse(m.Groups[2].Value);
                    day = int.Parse(m.Groups[3].Value);
                }
                else if ((m = NUMERIC_FORMAT.Match(arg)).Success)
                {
                    year = int.Parse(m.Groups[1].Value);
                    month = int.Parse(m.Groups[2].Value);
                    day = int.Parse(m.Groups[3].Value);
                }
                else if ((m = FRANCE_FORMAT.Match(arg)).Success)
                {
                    day = int.Parse(m.Groups[1].Value);
                    month = int.Parse(m.Groups[2].Value);
                    year = int.Parse(m.Groups[3].Value);
                }
                else
                {
                    throw new FormatException($"Cannot find date format for argument \"{arg}\"");
                }

                if (year < 50)
                    year = 2000 + year;
                else if (year < 100)
                    year = 1900 + year;

                var date = new DateTime(year, month, day);

                if (prevIsRange)
                {
                    if (prevDate == null)
                        throw new FormatException($"Incorrect date range specification");

                    DateTime intermediateDate;
                    if (prevDate.Value > date)
                    {
                        // Go backwards
                        intermediateDate = prevDate.Value.AddDays(-1);
                        while (intermediateDate >= date)
                        {
                            dates.Add(intermediateDate);
                            intermediateDate = intermediateDate.AddDays(-1);
                        }
                    }
                    else
                    {
                        // Go forward
                        intermediateDate = prevDate.Value.AddDays(1);
                        while (intermediateDate <= date)
                        {
                            dates.Add(intermediateDate);
                            intermediateDate = intermediateDate.AddDays(1);
                        }
                    }

                    prevIsRange = false;
                }
                else
                {
                    dates.Add(date);
                }

                prevDate = date;
            }

            return dates;
        }

        /// <summary>
        /// Indicates that a string is not empty and contains only digit characters.
        /// </summary>
        public static bool HasOnlyDigits(string str)
        {
            if (str == null || str == "")
                return false;

            foreach (var c in str)
            {
                if (!char.IsDigit(c))
                    return false;
            }
            return true;
        }

        /// <summary>
        /// Return the given connection string with hidden password
        /// </summary>
        public static string HideConnectionStringPassword(string connectionString)
        {
            return Regex.Replace(connectionString, @"(.*;)?Password=([^;]+)(;.*)?", "$1Password=<hidden>$3");
        }
    }
}
