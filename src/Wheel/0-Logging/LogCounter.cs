﻿using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Net;
using System.Threading;

namespace Wheel
{
    /// <summary>
    /// An implementation of <see cref="IReporter"/> that only counts the number of log messages emitted per log level.
    /// </summary>
    public class LogCounter : IReporter
    {
        protected readonly LogLevel _countLevel;

        public LogCounter(LogLevel countLevel = LogLevel.Trace)
        {
            _countLevel = countLevel;
            Count = new Dictionary<LogLevel, int>();
            Clear();
        }

        public virtual void Clear()
        {
            foreach (LogLevel l in Enum.GetValues(typeof(LogLevel)))
            {
                if (IsEnabled(l))
                    Count[l] = 0;
            }
        }

        public Dictionary<LogLevel, int> Count { get; }
        public int CountCriticals => Count[LogLevel.Critical];
        public int CountErrors => Count[LogLevel.Error];
        public int CountWarnings => Count[LogLevel.Warning];
        public int CountProblems => CountCriticals + CountErrors + CountWarnings;

        public IDisposable BeginScope<TState>(TState state)
        {
            return null;
        }

        public bool IsEnabled(LogLevel logLevel)
        {
            return logLevel >= _countLevel;
        }

        public virtual void Log<TState>(LogLevel logLevel, EventId eventId, TState state, Exception exception, Func<TState, Exception, string> formatter)
        {
            if (logLevel < _countLevel)
                return;
            Count[logLevel]++;
        }
    }
}
