﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;

namespace Wheel
{
    public static class ValidationUtils
    {
        /// <summary>
        /// Validate an object outside controllers, pages, views or options mechanisms.
        /// <para>
        /// Return false in case of validation error.
        /// </para>
        /// </summary>
        public static bool IsValid<T>(T entity)
        {
            if (entity == null)
                throw new ArgumentNullException(nameof(entity));

            var context = new ValidationContext(entity);
            var results = new List<ValidationResult>();
            return Validator.TryValidateObject(entity, context, results);
        }

        /// <summary>
        /// Validate an object outside controllers, pages, views or options mechanisms.
        /// <para>
        /// Throw a <see cref="ValidationException"/> in case of validation error.
        /// </para>
        /// </summary>
        public static T Validate<T>(T entity)
        {
            if (entity == null)
                throw new ArgumentNullException(nameof(entity));

            var context = new ValidationContext(entity);
            var results = new List<ValidationResult>();
            var isValid = Validator.TryValidateObject(entity, context, results);

            if (!isValid)
            {
                var message = $"{results.Count} validation error{(results.Count > 1 ? "s" : "")} on entity {entity.GetType().Name}:\n"
                    + string.Join("\n", results.Select(x => $"- {x.ErrorMessage}").ToArray());

                throw new ValidationException(message);
            }

            return entity;
        }
    }
}
