﻿using System;

namespace Wheel
{
    public static class ConvertUtils
    {
        /// <summary>
        /// Convert an object to the specified type.
        /// </summary>
        public static object To(object value, Type type)
        {
            if (type == null)
                throw new ArgumentNullException(nameof(type));

            if (value == null)
                return type.GetDefaultValue();

            var underlyingType = Nullable.GetUnderlyingType(type);
            if (underlyingType != null)
            {
                type = underlyingType;
            }

            if (value is string s)
            {
                if (type.IsEnum)
                    return Enum.Parse(type, s);
            }
           
            return Convert.ChangeType(value, type);
        }

        public static T To<T>(object value)
        {
            return (T) To(value, typeof(T));
        }
    }
}