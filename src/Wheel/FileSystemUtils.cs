﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using System.IO;

namespace Wheel
{
    /// <summary>
    /// Helper methods related to the file system.
    /// </summary>
    public static class FileSystemUtils
    {
        /// <summary>
        /// List the given <paramref name="relativePaths"/> that exist in <paramref name="dir"/>.
        /// </summary>
        /// <param name="dir">The directory where to search.</param>
        /// <param name="relativePaths">File or directory relative paths.</param>
        /// <returns></returns>
        public static string[] Exist(string dir, params string[] relativePaths)
        {
            if (dir == null)
                throw new ArgumentNullException(nameof(dir));
            if (!Directory.Exists(dir))
                throw new ArgumentException($"{nameof(dir)} ({dir}) does not exist", nameof(dir));
            if (relativePaths == null)
                throw new ArgumentNullException(nameof(relativePaths));
            if (relativePaths.Length == 0)
                throw new ArgumentException($"{nameof(relativePaths)} cannot be empty", nameof(relativePaths));

            var list = new List<string>(relativePaths.Length);
            foreach (var relativePath in relativePaths)
            {
                if (Path.IsPathRooted(relativePath))
                    throw new ArgumentException($"{nameof(relativePaths)} contains an absolute path ({relativePath})", nameof(relativePaths));
                var fullPath = Path.Combine(dir, relativePath);
                if (File.Exists(fullPath) || Directory.Exists(fullPath))
                    list.Add(relativePath);
            }
            return list.ToArray();
        }

        /// <summary>
        /// List the given <paramref name="relativePaths"/> that does not exist in <paramref name="dir"/>.
        /// </summary>
        /// <param name="dir">The directory where to search.</param>
        /// <param name="relativePaths">File or directory relative paths.</param>
        /// <returns></returns>
        public static string[] NotExist(string dir, params string[] relativePaths)
        {
            var exist = Exist(dir, relativePaths);
            var list = new List<string>(relativePaths.Length);
            foreach (var relativePath in relativePaths)
            {
                if (! exist.Contains(relativePath))
                    list.Add(relativePath);
            }
            return list.ToArray();
        }

        /// <summary>
        /// Search a directory recursively, backwards from <paramref name="startDir"/>, until one of the <paramref name="stopPaths"/> is found.
        /// </summary>
        /// <param name="startDir">Directory from which the search is performed.</param>
        /// <param name="stopPaths">The search is stopped as soon as one of these paths exist.</param>
        /// <returns>Full path of the paths found.</returns>
        public static List<string> SearchBackwardsFrom(string startDir, params string[] stopPaths)
            => SearchBackwardsFrom(startDir, (IEnumerable<string>) stopPaths);

        /// <summary>
        /// Search a directory recursively, backwards from <paramref name="startDir"/>, until one of the <paramref name="stopPaths"/> is found.
        /// </summary>
        /// <param name="startDir">Directory from which the search is performed.</param>
        /// <param name="stopPaths">The search is stopped as soon as one of these paths exist.</param>
        /// <param name="includePaths">These paths are included in the return value when they exist, but the search is not stopped.</param>
        /// <param name="additionnalDir">Add this subdirectory in stopPaths at each recursion, only if none of the stopPaths have been found for this recursion.</param>
        /// <returns>Full path of the paths found.</returns>
        public static List<string> SearchBackwardsFrom(string startDir, IEnumerable<string> stopPaths, IEnumerable<string> includePaths = null, string additionnalDir = null)
        {
            if (startDir == null)
                throw new ArgumentNullException(nameof(startDir));
            if (! Directory.Exists(startDir))
                throw new ArgumentException($"{nameof(startDir)} ({startDir}) does not exist", nameof(startDir));
            if (stopPaths == null)
                throw new ArgumentNullException(nameof(stopPaths));
            if (stopPaths.Count() == 0)
                throw new ArgumentException($"{nameof(stopPaths)} cannot be empty", nameof(stopPaths));
            if (includePaths == null)
                includePaths = new List<string>(0);

            foreach (var stopPath in stopPaths)
                if (Path.IsPathRooted(stopPath))
                    throw new ArgumentException($"{nameof(stopPaths)} contains an absolute path ({stopPath})", nameof(stopPaths));
            
            foreach (var includePath in includePaths)
                if (Path.IsPathRooted(includePath))
                    throw new ArgumentException($"{nameof(includePaths)} contains an absolute path ({includePath})", nameof(includePaths));

            startDir = startDir.TrimEnd(Path.DirectorySeparatorChar, Path.AltDirectorySeparatorChar);

            var results = new List<string>();
            DoSearchBackwardsFrom(results, Path.GetFullPath(startDir), stopPaths, includePaths, additionnalDir);
            return results;
        }

        // - dir: existing full path.
        // - relativePaths: only relative paths (checked).
        private static void DoSearchBackwardsFrom(List<string> results, string dir, IEnumerable<string> stopPaths, IEnumerable<string> includePaths, string additionnalDir)
        {
            bool stop = false;

            // Current dir
            foreach (var stopPath in stopPaths)
            {
                var fullPath = Path.Combine(dir, stopPath);
                if (File.Exists(fullPath) || Directory.Exists(fullPath))
                {
                    results.Add(fullPath);
                    stop = true;
                }
            }

            foreach (var includePath in includePaths)
            {
                var fullPath = Path.Combine(dir, includePath);
                if (File.Exists(fullPath) || Directory.Exists(fullPath))
                {
                    results.Add(fullPath);
                }
            }

            if (stop)
                return;

            // Additional dir
            if (additionnalDir != null)
            {
                foreach (var stopPath in stopPaths)
                {
                    var fullPath = Path.Combine(Path.Combine(dir, additionnalDir), stopPath);
                    if (File.Exists(fullPath) || Directory.Exists(fullPath))
                    {
                        results.Add(fullPath);
                        stop = true;
                    }
                }

                foreach (var includePath in includePaths)
                {
                    var fullPath = Path.Combine(Path.Combine(dir, additionnalDir), includePath);
                    if (File.Exists(fullPath) || Directory.Exists(fullPath))
                    {
                        results.Add(fullPath);
                    }
                }

                if (stop)
                    return;
            }

            // Not found
            dir = Path.GetDirectoryName(dir);
            if (string.IsNullOrEmpty(dir))
                return;
            else
                DoSearchBackwardsFrom(results, dir, stopPaths, includePaths, additionnalDir); // Recursivity
        }
    }
}
